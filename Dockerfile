FROM openjdk:8
COPY src/main/resources/application.yml /root/.m2/repository/com/mindzen/infra/
COPY src/main/resources/application.yml $HOME/
COPY src/main/resources/RWOperations.txt /home/mindzen/RWOperations.java
COPY plugin/ /root/.m2/repository/com/mindzen/
COPY src/main/resources/trigger.sh /home/mindzen/trigger.sh
COPY build/libs/user-license-0.0.1.jar user-license-0.0.1.jar
EXPOSE 9903
ENTRYPOINT ["java" , "-jar", "user-license-0.0.1.jar"]