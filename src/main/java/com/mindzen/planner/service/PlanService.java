package com.mindzen.planner.service;

import java.util.List;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.PlanDTO;

public interface PlanService {

	public MSAResponse getPlans(boolean paid, Integer pageNo, Integer pageSize, Long id);

	public MSAResponse createPlan(PlanDTO planDTO);

	public MSAResponse updatePlan(PlanDTO planDTO);

	public MSAResponse deletePlan(List<Long> planId);


}
