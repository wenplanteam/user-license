package com.mindzen.planner.service;

import java.util.List;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.CompanyDTO;

public interface CompanyService {

	public MSAResponse companyList(Long id, Boolean createdBy, Integer pageNo, Integer pageSize);

	public MSAResponse updateCompany(CompanyDTO companyDTO);

	public MSAResponse inActivateCompany(List<Long> companyId);

	MSAResponse insertCompany(CompanyDTO companyDTO);


}
