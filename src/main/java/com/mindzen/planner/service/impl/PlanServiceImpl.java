package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.AmountDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.entity.CurrencyMaster;
import com.mindzen.planner.entity.PeriodicMaster;
import com.mindzen.planner.entity.Plan;
import com.mindzen.planner.repository.CurrencyMasterRepository;
import com.mindzen.planner.repository.PeriodicMasterRepository;
import com.mindzen.planner.repository.PlanRepository;
import com.mindzen.planner.service.PlanService;

@Component
public class PlanServiceImpl implements PlanService{

	PlanRepository planRepository;
	CurrencyMasterRepository currencyMasterRepository;
	PeriodicMasterRepository PeriodicMasterRepository;

	public PlanServiceImpl(PlanRepository planRepository, CurrencyMasterRepository currencyMasterRepository,
			PeriodicMasterRepository PeriodicMasterRepository) {
		this.planRepository = planRepository;
		this.currencyMasterRepository = currencyMasterRepository;
		this.PeriodicMasterRepository = PeriodicMasterRepository;
	}

	@Override
	public MSAResponse getPlans(boolean paid, Integer pageNo, Integer pageSize, Long id) {
		if(null == pageNo)
			pageNo = 1;
		if(null == pageSize)
			pageSize = 10;
		PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
		MSAResponse response ;
		Page<Plan> planPage = null ;
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		if(true == paid) {
			planPage = planRepository.findByActiveAndPaid(true,paid,pageRequest);
			pagination.setTotalRecords(planPage.getTotalElements());
			response = new MSAResponse(true,SUCS,planListToPlan(planPage.getContent()),pagination,HttpStatus.OK);
		}
		else if(null != id) {
			Plan plan = planRepository.findByIdAndActive(id,true);
			pagination.setTotalRecords(1L);
			response = new MSAResponse(true,SUCS,mapPlanEntityToDTO(plan),pagination,HttpStatus.OK);
		}
		else {
			planPage = planRepository.findByActive(true,pageRequest);
			pagination.setTotalRecords(planPage.getTotalElements());
			response = new MSAResponse(true,SUCS,planListToPlan(planPage.getContent()),pagination,HttpStatus.OK);
		}
		return response;
	}

	private List<PlanDTO> planListToPlan(List<Plan> planList){
		List<PlanDTO> planDTOList = new ArrayList<>();
		if(null != planList && !planList.isEmpty()) {
			planList.forEach(plan -> {
				PlanDTO planDTO =  mapPlanEntityToDTO(plan);
				planDTOList.add(planDTO);
			});
		}
		return planDTOList;
	}

	private PlanDTO mapPlanEntityToDTO(Plan plan) {
		PlanDTO planDTO = new PlanDTO();
		planDTO.setId(plan.getId());
		planDTO.setName(plan.getName());
		planDTO.setActive(plan.isActive());
		AmountDTO amountDTO = new AmountDTO();
		amountDTO.setMonthly(plan.getMonthlyAmount());
		amountDTO.setAnnually(plan.getAnnuallyAmount());
		planDTO.setAmount(amountDTO);
		planDTO.setValidityPeriod(plan.getValidityPeriod());
		planDTO.setMaxLicenses(plan.getMaxLicenses());
		planDTO.setProjectsAllowed(plan.getProjectsAllowed());
		planDTO.setPaid(plan.isPaid());
		if(null != plan.getCurrencyMaster()) {
			planDTO.setCurrencyId(plan.getCurrencyMaster().getId());
			planDTO.setCurrencyName(plan.getCurrencyMaster().getName());
		}
		if(null != plan.getPeriodicMaster()) {
			planDTO.setPeriodicId(plan.getPeriodicMaster().getId());
			planDTO.setPeriodicName(plan.getPeriodicMaster().getName());
		}
		planDTO.setLicensesNos(0L);
		planDTO.setProjectsAllowed(plan.getProjectsAllowed());
		return planDTO;
	}

	@Override
	public MSAResponse createPlan(PlanDTO planDTO) {
		if(null ==planDTO.getCurrencyId())
			planDTO.setCurrencyId(1L);
		if(null ==planDTO.getPeriodicId())
			planDTO.setPeriodicId(1L);
		CurrencyMaster currencyMaster = currencyMasterRepository.findById(planDTO.getCurrencyId()).get();
		PeriodicMaster periodicMaster = PeriodicMasterRepository.findById(planDTO.getPeriodicId()).get(); 
		if(null != planDTO.getAmount()) {
			Plan plan = new Plan();
			plan.setCurrencyMaster(currencyMaster);
			plan.setPeriodicMaster(periodicMaster);
			plan.setName(planDTO.getName());
			plan.setActive(true);
			plan.setPaid(true);
			plan.setMonthlyAmount(planDTO.getAmount().getMonthly());
			plan.setAnnuallyAmount(planDTO.getAmount().getAnnually());
			plan.setValidityPeriod(1L);
			plan.setMaxLicenses(25L);
			plan.setProjectsAllowed(planDTO.getProjectsAllowed());
			planRepository.save(plan);
			return new MSAResponse(true , HttpStatus.OK, "Plan created successfully", null);
		}
		else
			return new MSAResponse(false , HttpStatus.BAD_REQUEST, ERR, null);
	}

	@Override
	public MSAResponse updatePlan(PlanDTO planDTO) {
		Optional<Plan> planOptional = planRepository.findById(planDTO.getId());
		if(planOptional.isPresent()) {
			Plan plan = planOptional.get();
			plan.setName(planDTO.getName());
			plan.setMonthlyAmount(planDTO.getAmount().getMonthly());
			plan.setAnnuallyAmount(planDTO.getAmount().getAnnually());
			plan.setProjectsAllowed(planDTO.getProjectsAllowed());
			planRepository.save(plan);
			return new MSAResponse(true , HttpStatus.OK, "Plan updated successfully", null);
		}
		else 
			return new MSAResponse(true , HttpStatus.OK, "Plan not found", null);
	}

	@Override
	public MSAResponse deletePlan(List<Long> planId) {
		List<Plan> planList = planRepository.findAllById(planId);
		if(!planList.isEmpty()) {
			planList.forEach(plan -> plan.setActive(false));
			planRepository.saveAll(planList);
			return new MSAResponse(true , HttpStatus.OK, "Plan(s) deleted successfully", null);
		}
		else 
			return new MSAResponse(true , HttpStatus.OK, "Plan not found", null);
	}

}
