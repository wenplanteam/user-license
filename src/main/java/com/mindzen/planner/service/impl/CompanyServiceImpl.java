package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.FND;
import static com.mindzen.infra.api.response.ApiResponseConstants.NOT;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.CompanyUsers;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.repository.CompanyRepository;
import com.mindzen.planner.repository.CompanyUsersRepository;
import com.mindzen.planner.repository.UserRepository;
import com.mindzen.planner.service.CompanyService;

@Component
public class CompanyServiceImpl implements CompanyService {

	CompanyRepository companyRepository;
	UserRepository userRepository;
	CompanyUsersRepository companyUsersRepository;

	MSAResponse msaResponse = null; 

	public CompanyServiceImpl(CompanyRepository companyRepository, UserRepository userRepository,
			CompanyUsersRepository companyUsersRepository) {
		this.companyRepository = companyRepository;
		this.userRepository = userRepository;
		this.companyUsersRepository = companyUsersRepository;
	}

	@Override
	public MSAResponse companyList(Long id, Boolean createdBy, Integer pageNo, Integer pageSize) {
		if(null == pageNo)
			pageNo = 1;
		if(null == pageSize)
			pageSize = 1000;
		PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
		List<Company> companyList = new ArrayList<>();
		Long userId = UserInfoContext.getUserThreadLocal();
		User user = userRepository.findById(userId).get();
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		if(null != createdBy && createdBy.equals(true)) {
			List<Long> companyIdList = new ArrayList<>(); 
			companyIdList = companyRepository.findByCreatedByAndIsActive(userId,true);
			Long userCreatedBy = userRepository.findCreatedByByUserId(userId);
			CompanyUsers companyUser = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user,userCreatedBy);
			companyIdList.add(companyUser.getCompany().getId());
			Page<Company> companyPage = companyRepository.findAllCompanyByIds(companyIdList, pageRequest);
			companyList = companyPage.getContent();
			pagination.setTotalRecords(companyPage.getTotalElements());
			List<CompanyDTO> companyDTOList =  new ArrayList<>();
			companyList.forEach(company -> {
				CompanyDTO companyDTO = CompanyEntityToCompanyDTO(company);
				companyDTOList.add(companyDTO);
			});
			msaResponse = new MSAResponse(true, REC + FND, companyDTOList, pagination, HttpStatus.OK);
		}
		else if(null == id) {
			Page<Company> companyPage = companyRepository.getActiveCompanies(pageRequest);
			companyList = companyPage.getContent();
			pagination.setTotalRecords(companyPage.getTotalElements());
			List<CompanyDTO> companyDTOList =  new ArrayList<>();
			companyList.forEach(company -> {
				CompanyDTO companyDTO = CompanyEntityToCompanyDTO(company);
				companyDTOList.add(companyDTO);
			});
			msaResponse = new MSAResponse(true, REC + FND, companyDTOList, pagination, HttpStatus.OK);
		}
		else {
			Optional<Company> companyOptional = companyRepository.findById(id);
			if(companyOptional.isPresent()) {
				Company company =  companyOptional.get();
				CompanyDTO companyDTO = CompanyEntityToCompanyDTO(company);
				msaResponse = new MSAResponse(true, HttpStatus.OK,  REC + FND, companyDTO);
			}
			else 
				msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, NOT + FND, "E-001", null);
		}
		return msaResponse ;
	}

	private CompanyDTO CompanyEntityToCompanyDTO(Company company) {
		ModelMapper mapper =  new ModelMapper();
		CompanyDTO companyDTO =  new CompanyDTO();
		List<CompanyUsers> companyusersList = companyUsersRepository.findByCompany(company);
		List<Long> companyAvailabilityList = companyRepository.findProjectCompanyIdByCompanyId(company.getId());
		if(companyusersList.isEmpty() && companyAvailabilityList.isEmpty())
			companyDTO.setDelete(true);
		else
			companyDTO.setDelete(false);
		mapper.map(company, companyDTO);
		return companyDTO;
	}

	@Override
	public MSAResponse insertCompany(CompanyDTO companyDTO) {
		if(null != companyDTO.getName()) {
			Long userId = UserInfoContext.getUserThreadLocal();
			List<Long> companyIds = companyRepository.findByCreatedByAndIsActive(userId, true);
			Long createdBy = userRepository.findCreatedByByUserId(userId);
			List<CompanyUsers> companyUsers = companyUsersRepository.findByUserIdAndCreatedBy(Arrays.asList(userId), createdBy);
			companyIds.add(companyUsers.get(0).getCompany().getId());
			List<Company> companies = companyRepository.findAllById(companyIds);
			Long count = companies.stream().filter(company -> company.getName().equals(companyDTO.getName())).count();
			if (count > 0)
				return new MSAResponse(false, HttpStatus.BAD_REQUEST,"Company Already Exist", "E-001", null);
			else {
				Company company = new Company();
				company.setIsActive(true);
				msaResponse = companyInfoUpdate(company, companyDTO);	
			}
		}
		else
			msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND,"Company Name Not Found", "E-001", null);
		return msaResponse;

	}

	@Override
	public MSAResponse updateCompany(CompanyDTO companyDTO) {
		if(null != companyDTO.getId()) {
			Optional<Company> companyOptional = companyRepository.findById(companyDTO.getId());
			if(companyOptional.isPresent()){
				if(companyDTO.getName().equals(companyOptional.get().getName()))
					return companyInfoUpdate(companyOptional.get(), companyDTO);
				Long userId = UserInfoContext.getUserThreadLocal();
				List<Long> companyIds = companyRepository.findByCreatedByAndIsActive(userId, true);
				Long createdBy = userRepository.findCreatedByByUserId(userId);
				List<CompanyUsers> companyUsers = companyUsersRepository.findByUserIdAndCreatedBy(Arrays.asList(userId), createdBy);
				companyIds.add(companyUsers.get(0).getCompany().getId());
				List<Company> companies = companyRepository.findAllById(companyIds);
				Long count =0L;
				for (Company company : companies) {
					if(company.getName().equals(companyDTO.getName()))
						count = count + 1;
				}
				if (count > 0)
					return new MSAResponse(false, HttpStatus.BAD_REQUEST,"Company Already Exist", "E-001", null);
				else if(null != companyDTO.getName())
					msaResponse = companyInfoUpdate(companyOptional.get(), companyDTO);
				else
					msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND,"Company Name Not Found", "E-001", null);
			}
			else
				msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, "Company " +NOT + FND, "E-001", null);
		}
		return msaResponse;
	}

	private MSAResponse companyInfoUpdate(Company company, CompanyDTO companyDTO) {
		MSAResponse msaResponse2;
		companyDTOtoENTITY(company,companyDTO);
		companyRepository.save(company);
		msaResponse2 = new MSAResponse(true, HttpStatus.OK,  SUCS, null);
		return msaResponse2;
	}

	private Company companyDTOtoENTITY(Company company, CompanyDTO companyDTO) {
		if(null != companyDTO.getName()) {
			company.setName(companyDTO.getName());
		}
		if(null != companyDTO.getEmail()) {
			company.setEmail(companyDTO.getEmail().toLowerCase());
		}
		if(null != companyDTO.getContactNo()) {
			company.setContactNo(companyDTO.getContactNo());
		}
		if(null != companyDTO.getAddress())	{
			company.setAddress(companyDTO.getAddress());
		}
		if(null != companyDTO.getContactFirstName())	{
			company.setContactFirstName(companyDTO.getContactFirstName());
		}
		if(null != companyDTO.getContactLastName())	{
			company.setContactLastName(companyDTO.getContactLastName());
		}
		return company;
	}

	@Override
	public MSAResponse inActivateCompany(List<Long> companyId) {
		List<Company> companies = companyRepository.findAllById(companyId);
		if(null != companies) {
			companies.forEach( company -> company.setIsActive(false));
			companyRepository.saveAll(companies);
			msaResponse = new MSAResponse(true, HttpStatus.OK,  SUCS, null);
		}
		else
			msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, NOT + FND, "E-001", null);
		return msaResponse;
	}

}
