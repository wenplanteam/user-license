package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.LicenseCountDTO;
import com.mindzen.planner.dto.LicenseDTO;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.PaymentDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserPlanDetailDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;
import com.mindzen.planner.entity.CurrencyMaster;
import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.LicenseDistribution;
import com.mindzen.planner.entity.LicenseDistributionHistory;
import com.mindzen.planner.entity.LicenseHistory;
import com.mindzen.planner.entity.Plan;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.entity.UserLicense;
import com.mindzen.planner.entity.UserLicenseHistory;
import com.mindzen.planner.entity.UserPlanDetail;
import com.mindzen.planner.entity.UserPlanMaster;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.CurrencyMasterRepository;
import com.mindzen.planner.repository.LicenseDistributionHistoryRepository;
import com.mindzen.planner.repository.LicenseDistributionRepository;
import com.mindzen.planner.repository.LicenseHistoryRepository;
import com.mindzen.planner.repository.LicenseRepository;
import com.mindzen.planner.repository.PeriodicMasterRepository;
import com.mindzen.planner.repository.PlanRepository;
import com.mindzen.planner.repository.UserLicenseHistoryRepository;
import com.mindzen.planner.repository.UserLicenseRepository;
import com.mindzen.planner.repository.UserPlanDetailRepository;
import com.mindzen.planner.repository.UserPlanMasterRepository;
import com.mindzen.planner.repository.UserRepository;
import com.mindzen.planner.service.LicenseService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Naveenkumar boopathi
 *
 */
@Component
@Slf4j
public class LicenseServiceImpl implements LicenseService {

	CurrencyMasterRepository currencyMasterRepository;
	UserPlanMasterRepository userPlanMasterRepository;
	UserPlanDetailRepository userPlanDetailRepository;
	UserLicenseRepository userLicenseRepository;
	UserLicenseHistoryRepository userLicenseHistoryRepository;
	UserRepository userRepository;
	PlanRepository planRepository;
	PeriodicMasterRepository periodicMasterRepository;
	LicenseRepository licenseRepository;
	LicenseHistoryRepository licenseHistoryRepository;
	LicenseDistributionRepository licenseDistributionRepository;
	LicenseDistributionHistoryRepository licenseDistributionHistoryRepository;
	MessageQueueService messageQueueService;
	AppConfig appConfig;

	@Value("${newLicenseCreationEmail}")
	private String newLicenseCreationEmail;

	@Value("${stripePublicKey}")
	private String stripePublicKey;

	@Value("${stripeSecrectKey}")
	private String stripeSecretKey;

	@PostConstruct
	public void init() {
		Stripe.apiKey = stripeSecretKey;
	}


	public LicenseServiceImpl(UserPlanMasterRepository userPlanMasterRepository, UserRepository userRepository,
			CurrencyMasterRepository currencyMasterRepository, PeriodicMasterRepository periodicMasterRepository,
			LicenseRepository licenseRepository, LicenseHistoryRepository licenseHistoryRepository,
			UserPlanDetailRepository userPlanDetailRepository, PlanRepository planRepository,
			LicenseDistributionRepository licenseDistributionRepository, UserLicenseHistoryRepository userLicenseHistoryRepository,
			UserLicenseRepository userLicenseRepository, LicenseDistributionHistoryRepository licenseDistributionHistoryRepository,
			MessageQueueService messageQueueService, AppConfig appConfig) {
		this.userPlanMasterRepository= userPlanMasterRepository;
		this.userRepository = userRepository;
		this.currencyMasterRepository = currencyMasterRepository;
		this.periodicMasterRepository = periodicMasterRepository;
		this.licenseRepository = licenseRepository;
		this.licenseHistoryRepository = licenseHistoryRepository;
		this.userPlanDetailRepository = userPlanDetailRepository;
		this.planRepository = planRepository;
		this.licenseDistributionRepository = licenseDistributionRepository;
		this.licenseDistributionHistoryRepository = licenseDistributionHistoryRepository;
		this.userLicenseRepository = userLicenseRepository;
		this.userLicenseHistoryRepository = userLicenseHistoryRepository;
		this.messageQueueService = messageQueueService;
		this.appConfig = appConfig;
	}


	/* 
	 * payment initiated and record insert into database
	 */
	@Override
	public MSAResponse buyLicense(UserPlanMasterDTO userPlanMasterDTO) {
		if(null != userPlanMasterDTO.getAmount()) {
			Long userId = UserInfoContext.getUserThreadLocal();
			MSAResponse response = null;
			if(null ==userPlanMasterDTO.getCurrencyId())
				userPlanMasterDTO.setCurrencyId(1L);
			if(null ==userPlanMasterDTO.getPeriodicId())
				userPlanMasterDTO.setPeriodicId(1L);
			User user = userRepository.findById(userId).get();
			CurrencyMaster currencyMaster = currencyMasterRepository.findById(userPlanMasterDTO.getCurrencyId()).get();
			//PeriodicMaster periodicMaster = periodicMasterRepository.findById(userPlanMasterDTO.getPeriodicId()).get();
			UserPlanMaster userPlanMaster = new UserPlanMaster();
			userPlanMaster.setAmount(userPlanMasterDTO.getAmount());
			userPlanMaster.setTransactionDate(LocalDateTime.now());
			String transactionId = wenplanTransactionCode();
			userPlanMaster.setTransactionId(transactionId);
			userPlanMaster.setPaymentStatus(Constants.PAYMENT_INITIATED);
			userPlanMaster.setPurchasedBy(user);
			userPlanMaster.setCurrencyId(currencyMaster);
			userPlanMaster = userPlanMasterRepository.save(userPlanMaster);
			if(!userPlanMasterDTO.getPlanDTOs().isEmpty()) {
				UserPlanMaster masterInfo = userPlanMaster;
				userPlanMasterDTO.getPlanDTOs().forEach(plans ->{
					if(plans.getLicensesNos() > 0 && plans.getPrice().longValue() > 0) {
						Plan plan = planRepository.findById(plans.getId()).get();
						UserPlanDetail userPlanDetail = new UserPlanDetail();
						userPlanDetail.setLicensesNos(plans.getLicensesNos());
						userPlanDetail.setAmount(plans.getPrice());
						userPlanDetail.setBillingCycle(plans.getBillingCycle());
						userPlanDetail.setPlan(plan);
						userPlanDetail.setUserPlanMaster(masterInfo);
						userPlanDetail.setPeriodicMaster(plan.getPeriodicMaster());
						userPlanDetail.setActive(true);
						userPlanDetail.setCurrencyMaster(plan.getCurrencyMaster());
						userPlanDetail.setUsedLicense(0L);
						userPlanDetail= userPlanDetailRepository.save(userPlanDetail);
					}
				});
				BigDecimal value = new BigDecimal("100");
				BigDecimal chargeIncents = userPlanMasterDTO.getAmount().multiply(value);
				PaymentDTO payment = new PaymentDTO();
				payment.setAmount(chargeIncents);
				payment.setStripePublicKey(stripePublicKey);
				payment.setCurrency("USD");
				payment.setTransactionId(transactionId);
				response =  new MSAResponse(true, HttpStatus.OK, "Request send to Bank", payment);
			}
			else
				response =  new MSAResponse(false, HttpStatus.OK, " No Plans selected ", ERR, "Payment Error");
			return response;
		}
		else
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, " Amount Not Found ", null);
	}

	/* 
	 * get response from service provider  and create licenses
	 */
	@Override
	public MSAResponse licenseResponse(StripeDataDTO stripeData) {
		MSAResponse response = null;

		BigInteger value = new BigInteger("100");
		BigInteger chargeIncents = stripeData.getAmount().multiply(value);
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", chargeIncents);
		chargeParams.put("currency", StripeDataDTO.Currency.USD);
		chargeParams.put("description", stripeData.getTransactionId());
		chargeParams.put("source", stripeData.getStripeToken() );

		String transactionStatus = null;
		String stripeTransactionId = null;
		String transactionId = null;
		String receiptUrl = null;
		String message = null;
		try {
			Charge charge = Charge.create(chargeParams);
			log.info("payment status" + charge);
			transactionId = charge.getDescription();
			transactionStatus = charge.getStatus();
			stripeTransactionId = charge.getBalanceTransaction();
			receiptUrl = charge.getReceiptUrl();
		} catch (StripeException e) { 
			transactionStatus = "Failed";
			message = StringUtils.capitalize(e.getCode().replace("_", " ")+", "+e.getMessage());
		}

		if(transactionStatus.equals("succeeded")) {
			UserPlanMaster userPlanMaster = userPlanMasterRepository.findByTransactionId(transactionId);
			userPlanMaster.setPaymentStatus(Constants.SUCCESS);
			userPlanMaster.setModeOfPayment("visa card");
			userPlanMaster.setGoogleTransactionId(stripeTransactionId);
			userPlanMaster.setReferenceId(userPlanMaster);
			userPlanMasterRepository.save(userPlanMaster);
			List<UserPlanDetail> userPlanDetailsList = userPlanDetailRepository.findByUserPlanMaster(userPlanMaster);	
			List<Long> licenseNos = new ArrayList<>();

			userPlanDetailsList.forEach(userPlanDetail -> {
				licenseNos.add(userPlanDetail.getLicensesNos());
				for(int i= 0 ; i <userPlanDetail.getLicensesNos() ;i++) {
					License license  = new License();
					String code = licenseTrnsactionCode();
					license.setCode(code);
					license.setStartDate(LocalDateTime.now());

					if(Constants.ANNUALLY.equalsIgnoreCase(userPlanDetail.getBillingCycle()))
						license.setExpiryDate(LocalDateTime.now().plusYears(1)); 
					else
						if(Constants.MONTHLY.equalsIgnoreCase(userPlanDetail.getBillingCycle()))
							license.setExpiryDate(LocalDateTime.now().plusDays(30));
						else
							license.setExpiryDate(LocalDateTime.now());
					license.setUserPlanDetail(userPlanDetail);
					license = licenseRepository.save(license);
					LicenseHistory licenseHistory = new LicenseHistory();
					licenseHistory.setCode(license.getCode());
					licenseHistory.setExpiryDate(license.getExpiryDate());
					licenseHistory.setLicenseId(license);
					licenseHistory.setStartDate(license.getStartDate());
					licenseHistory.setUserPlanDetail(userPlanDetail);
					licenseHistoryRepository.save(licenseHistory);
				}
			});
			Long licenseCount = 0L;
			for (Long count : licenseNos) {
				licenseCount += count;
			}
			userPlanMaster.getPurchasedBy().setEmail(userPlanMaster.getPurchasedBy().getEmail().toLowerCase());
			sendMail(newLicenseCreationEmail, "purchase_license_template.html",
					userPlanMaster.getPurchasedBy().getEmail(),"Wenplan Purchase License" ,null, null);
			sendMail(userPlanMaster.getPurchasedBy().getEmail(), "Purchase_license_receipt_template.html", 
					null, "Wenplan License Receipt", null,receiptUrl);
			Map<String,String > licenseResponse = new HashMap<>();
			licenseResponse.put("transactionId", transactionId);
			licenseResponse.put("noOfLicenses", ""+licenseCount);
			response =  new MSAResponse(true, HttpStatus.OK, "License created successfully", licenseResponse);
		}
		else {
			UserPlanMaster userPlanMaster = userPlanMasterRepository.findByTransactionId(stripeData.getTransactionId());
			userPlanMaster.setPaymentStatus("Failed");
			userPlanMasterRepository.save(userPlanMaster);
			response =  new MSAResponse(false, HttpStatus.OK, message , ERR, "Payment Error");
		}
		return response;
	}

	/**
	 * generate wenplan transaction code
	 */
	private String wenplanTransactionCode() {
		StringBuilder sb = new StringBuilder();
		String curMilSec = Long.toString(System.currentTimeMillis());
		sb.append("WP-"+ curMilSec);
		return sb.toString();
	}

	private String licenseTrnsactionCode() {
		StringBuilder sb = new StringBuilder();
		String curMilSec = Long.toString(System.currentTimeMillis());
		sb.append("Lic-" +  curMilSec );
		return sb.toString();
	}

	/*
	 * get user bought Licenses
	 */
	@Override
	public MSAResponse userLicense(Integer pageNo, Integer pageSize, String status) {
		if(null == pageNo)
			pageNo = 1;
		if(null == pageSize)
			pageSize = 10;
		PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
		MSAResponse response;
		Long userId = UserInfoContext.getUserThreadLocal();
		String userTimeZone = userRepository.findTimeZoneByUserId(userId);
		User user = userRepository.findById(userId).get();
		List<LicenseDTO> licenseDTOList = new ArrayList<>();
		UserPlanDetailDTO userPlanDetailDTO = new UserPlanDetailDTO();
		List<Long> userPlanMasterDetailId = userPlanMasterDetails(user);
		List<Long> userPlanDetailIds = userPlanDetail(userPlanMasterDetailId);
		if(userPlanDetailIds.isEmpty())
			userPlanDetailIds.add(0L);
		List<License> licenseListByUserPlanDetails = licenseRepository.findByUserPlanDetail(userPlanDetailIds);
		UserLicense userLicense = userLicenseRepository.findByUserAndActive(user, true);
		List<Long> licenseIds  = new ArrayList<>();
		licenseIds = userLicense(licenseListByUserPlanDetails,userLicense);
		Page<License> licenseListPage;
		if(licenseIds.isEmpty())
			licenseIds.add(0L);
		if(null != status && status.equalsIgnoreCase(Constants.VALID)) 
			licenseListPage = licenseRepository.findLicensesAndValidWithPagination(licenseIds, pageRequest);
		else if(null != status && status.equalsIgnoreCase(Constants.EXPIRED))
			licenseListPage = licenseRepository.findLicensesAndExpiredWithPagination(licenseIds, pageRequest);
		else if(null != status && status.equalsIgnoreCase(Constants.OWNED)) {
			UserLicense userLicensePayLoad = null;
			licenseIds = userLicense(licenseListByUserPlanDetails,userLicensePayLoad);
			if(licenseIds.isEmpty())
				licenseIds.add(0L);
			licenseListPage = licenseRepository.findLicensesWithPagination(licenseIds, pageRequest);
		}
		else 
			licenseListPage = licenseRepository.findLicensesWithPagination(licenseIds, pageRequest);
		List<License> licenseList = licenseListPage.getContent();
		if(!licenseList.isEmpty()) {
			licenseList.forEach(license -> {
				LicenseDTO licenseDTO = new LicenseDTO();
				UserLicense userLicenseAvailability = userLicenseRepository.findByLicenseAndActive(license,true);
				if(null != userLicenseAvailability)
					licenseDTO.setGivenTo(userLicenseAvailability.getUser().getEmail());
				LocalDate expiryDate = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(license.getExpiryDate(), userTimeZone).toLocalDate();
				licenseDTO.setExpiryDate(expiryDate.toString());
				if(null == license.getUserPlanDetail().getUserPlanMaster())
					licenseDTO.setGotFrom(user.getEmail());
				else
					licenseDTO.setGotFrom(license.getUserPlanDetail().getUserPlanMaster().getPurchasedBy().getEmail());
				if(LocalDateTime.now().compareTo(license.getExpiryDate()) <0)
					licenseDTO.setStatus(Constants.VALID);
				else
					licenseDTO.setStatus(Constants.EXPIRED);
				licenseDTO.setLicenseId(license.getId());
				licenseDTO.setLicenseNo(license.getCode());
				if(license.getCode().contains("Lic-Free-"))
					licenseDTO.setTrial(true);
				licenseDTO.setDays(ChronoUnit.DAYS.between(LocalDateTime.now(), license.getExpiryDate()));
				licenseDTO.setPlanName(license.getUserPlanDetail().getPlan().getName());
				licenseDTOList.add(licenseDTO);
			});
			userPlanDetailDTO.setLicenses(licenseDTOList);
			Pagination pagination = new Pagination();
			pagination.setCurrentPageNumber(pageRequest.getPageNumber());
			pagination.setPaginationSize(pageRequest.getPageSize());
			pagination.setTotalRecords(licenseListPage.getTotalElements());
			response =  new MSAResponse(true, "License found", userPlanDetailDTO.getLicenses(), pagination, HttpStatus.OK);
		}
		else 
			response =  new MSAResponse(false, HttpStatus.NOT_FOUND, "No Records" , "E-001", null);
		return response;
	}

	private List<Long> userLicense(List<License> licenseList, UserLicense userLicense) {
		List<Long> licenseIds = new ArrayList<>();
		licenseList.forEach(license ->  {
			if(!license.getCode().contains("Free")) {
				licenseIds.add(license.getId());
			}
		});
		if(null != userLicense) {
			if(!userLicense.getLicense().getCode().contains("Free")) {
				licenseIds.add(userLicense.getLicense().getId());
			}
		}
		return licenseIds;
	}


	private List<Long> userPlanDetail(List<Long> userPlanMasterDetailId) {
		if(userPlanMasterDetailId.isEmpty())
			userPlanMasterDetailId.add(0L);
		List<UserPlanDetail> userPlanDetailList = userPlanDetailRepository.finAlldByUserPlanMasterId(userPlanMasterDetailId);
		List<Long> userPlanDetailIds = new ArrayList<>();
		userPlanDetailList.forEach(userPlanDetail -> userPlanDetailIds.add(userPlanDetail.getId()));
		return userPlanDetailIds;
	}


	private List<Long> userPlanMasterDetails(User user) {
		List<UserPlanMaster> userPlanMasterList = userPlanMasterRepository.findByPurchasedByAndPaymentStatus(user,Constants.SUCCESS);
		List<Long> userPlanMasterId = new ArrayList<>() ;
		userPlanMasterList.forEach(userPlanMaster -> userPlanMasterId.add(userPlanMaster.getId()) );
		return userPlanMasterId;
	}


	/* 
	 * providing license to user
	 */
	@Override
	public MSAResponse provideLicense(LicenseDistributionDTO licenseDistributionDTO) {
		MSAResponse response;
		Long userId = UserInfoContext.getUserThreadLocal();
		License license = licenseRepository.findByCode(licenseDistributionDTO.getLicenseNo());
		User providedTo = userRepository.findByEmail(licenseDistributionDTO.getEmail().toLowerCase());
		User providedBy = userRepository.findById(userId).get();
		if(null != providedTo) {
			UserLicense userLicenseAvailable = userLicenseRepository.findByUserAndActive(providedTo,true);
			if(null !=userLicenseAvailable) {
				if(userLicenseAvailable.getLicense().getCode().contains("Lic-Free-")) {
					userLicenseAvailable.setActive(false);
					userLicenseRepository.save(userLicenseAvailable);
				}
			}
			UserLicense userLicenseAvailability = userLicenseRepository.findByUserAndActive(providedTo,true);
			if(null == userLicenseAvailability) {
				LicenseDistribution licenseDistribution =  new LicenseDistribution();
				licenseDistribution.setLicense(license);
				licenseDistribution.setProvidedDate(LocalDateTime.now());
				licenseDistribution.setProvidedTo(providedTo);
				licenseDistribution.setTransferStatus(Constants.SUCCESS);
				licenseDistribution = licenseDistributionRepository.save(licenseDistribution);
				LicenseDistributionHistory licenseDistributionHistory =  new LicenseDistributionHistory();
				licenseDistributionHistory.setLicense(license);
				licenseDistributionHistory.setLicenseDistribution(licenseDistribution);
				licenseDistributionHistory.setProvidedBy(providedBy);
				licenseDistributionHistory.setProvidedDate(LocalDateTime.now());
				licenseDistributionHistory.setProvidedTo(providedTo);
				licenseDistributionHistory.setStatus(licenseDistribution.getTransferStatus());
				licenseDistributionHistoryRepository.save(licenseDistributionHistory);
				UserLicense userLicense = new UserLicense();
				userLicense.setActive(true);
				userLicense.setActiveFrom(LocalDateTime.now());
				userLicense.setLicense(license);
				userLicense.setUser(providedTo);
				userLicense = userLicenseRepository.save(userLicense);
				UserLicenseHistory userLicenseHistory = new UserLicenseHistory();
				userLicenseHistory.setActiveFrom(LocalDateTime.now());
				userLicenseHistory.setLicense(license);
				userLicenseHistory.setUserLicenseId(userLicense);
				userLicenseHistoryRepository.save(userLicenseHistory);
				UserPlanDetail userPlanDetail = userPlanDetailRepository.findById(license.getUserPlanDetail().getId()).get();
				userPlanDetail.setUsedLicense(userPlanDetail.getUsedLicense()+1);
				userPlanDetailRepository.save(userPlanDetail);
				response =  new MSAResponse(true, HttpStatus.OK, "License provided successfully", null);
				sendMail(providedTo.getEmail(), "provide_license_template.html",
						providedBy.getFirstName()+" "+providedBy.getLastName(),"Wenplan License Provide", 
						newLicenseCreationEmail, null);
			}
			else
				response =  new MSAResponse(true, HttpStatus.OK, "User Already Associated With Another License", null);
		}
		else {
			response =  new MSAResponse(true, HttpStatus.OK,"No Users Found",ERR," No Users found in wenplan");
		}
		return response;
	}


	@Override
	public MSAResponse revokeLicense(LicenseDistributionDTO licenseDistributionDTO) {
		MSAResponse response;
		Long userId = UserInfoContext.getUserThreadLocal();
		User providedBy = userRepository.findById(userId).get();
		License license = licenseRepository.findByCode(licenseDistributionDTO.getLicenseNo());
		User user = userRepository.findByEmail(licenseDistributionDTO.getEmail().toLowerCase());
		UserLicense userLicense = userLicenseRepository.findByLicenseAndUserAndActive(license, user, true);
		if(null != userLicense) {
			LicenseDistributionHistory licenseDistributionHistory = 
					licenseDistributionHistoryRepository.findByLicenseAndProvidedToAndStatus(license, user,Constants.SUCCESS);
			licenseDistributionHistory.setRevokedDate(LocalDateTime.now());
			licenseDistributionHistory.setStatus(Constants.REVOKED);
			licenseDistributionHistoryRepository.save(licenseDistributionHistory);
			userLicense.setActive(false);
			userLicense = userLicenseRepository.save(userLicense);
			UserLicenseHistory userLicenseHistory = userLicenseHistoryRepository.findByUserLicenseIdAndLicense(userLicense, license);
			userLicenseHistory.setActiveTo(LocalDateTime.now());
			userLicenseHistoryRepository.save(userLicenseHistory);
			response =  new MSAResponse(true, HttpStatus.OK, "License got back successfully", null);
			UserPlanDetail userPlanDetail = userPlanDetailRepository.findById(license.getUserPlanDetail().getId()).get();
			userPlanDetail.setUsedLicense(userPlanDetail.getUsedLicense()-1);
			userPlanDetailRepository.save(userPlanDetail);
			sendMail(user.getEmail(), "revoke_license_template.html",
					providedBy.getFirstName()+" "+providedBy.getLastName(),
					"Wenplan License Revoked",newLicenseCreationEmail, newLicenseCreationEmail);
		}
		else {
			response =  new MSAResponse(false, HttpStatus.BAD_REQUEST, "License Not associated with the User", null);
		}
		return response;
	}


	@Override
	public MSAResponse userLicenseCount() {
		Long userId = UserInfoContext.getUserThreadLocal();
		LicenseCountDTO licenseCountDTO = new LicenseCountDTO();
		licenseCountDTO.setPurchased(0L);
		licenseCountDTO.setUnUsed(0L);
		licenseCountDTO.setUsed(0L);
		User user = userRepository.findById(userId).get();
		List<UserPlanMaster> userPlanMasterList = userPlanMasterRepository.findByPurchasedByAndPaymentStatus(user,Constants.SUCCESS);
		if(!userPlanMasterList.isEmpty()) {
			userPlanMasterList.forEach(userPlanMaster -> {
				List<UserPlanDetail> userPlanDetailList = userPlanDetailRepository.findByUserPlanMaster(userPlanMaster);
				userPlanDetailList.forEach(userPlanDetail -> {
					licenseCountDTO.setPurchased(userPlanDetail.getLicensesNos() + licenseCountDTO.getPurchased());
					licenseCountDTO.setUsed(userPlanDetail.getUsedLicense()+ licenseCountDTO.getUsed());
				});
			});
			licenseCountDTO.setUnUsed(licenseCountDTO.getPurchased() - licenseCountDTO.getUsed());
		}
		return  new MSAResponse(true, HttpStatus.OK, SUCS, licenseCountDTO);
	}

	private void sendMail(String email, String templateFileName, String providedUserName, String mailSubject,
			String newLicenseCreationCCEmail, String receiptUrl) {
		String firstName = null;
		String expiryDate = null;
		messageQueueService.sendMail(email, templateFileName, appConfig.getNewUserType(),
				appConfig.getUserVerifyPath(), providedUserName,mailSubject ,newLicenseCreationCCEmail
				, firstName,receiptUrl,expiryDate);
	}

	@Override
	public MSAResponse userLicenseAvailabilityCheck(String email) {
		MSAResponse response;
		User user = userRepository.findByEmail(email.toLowerCase());
		user.setLastLoggedIn(Date.from(Instant.now()));
		userRepository.save(user);
		UserLicense userLicense = userLicenseRepository.findByUserAndActive(user, true);
		if(null != userLicense) {
			LicenseDTO licenseDTO = new LicenseDTO();
			licenseDTO.setDays(ChronoUnit.DAYS.between(LocalDateTime.now(), userLicense.getLicense().getExpiryDate()));
			if(userLicense.getLicense().getCode().contains("Free"))
				licenseDTO.setTrial(true);
			response = new MSAResponse(true, HttpStatus.OK, SUCS, licenseDTO);
		}
		else
			response = new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, null);
		return response;
	}


	@Scheduled(cron ="00 29 05 * * *")
	public void licenseDailyStatusUpdate() {
		log.info("service working");
		List<Long> licenseIds = new ArrayList<>();
		List<UserLicense> userLicenseList =userLicenseRepository.findExpiredLicenses();
		if(null != userLicenseList) {
			userLicenseList.forEach(userLicense -> {
				Long days = ChronoUnit.DAYS.between(LocalDateTime.now(), userLicense.getLicense().getExpiryDate());
				if(days <= 0) 
					licenseIds.add(userLicense.getId());
			});
			if (!licenseIds.isEmpty()) {
				userLicenseRepository.licenseStatusUpdate(licenseIds);
			}
		}
		log.info("license remvoed id "+ licenseIds.toString());
	}

	@Scheduled(cron ="00 15 05 * * *")
	public void licenseMailStatusSend()  {
		ArrayList<String> emailList =  new ArrayList<>();
		LocalDateTime currentDate = LocalDate.now().plusDays(1).atStartOfDay();
		LocalDateTime startDate = LocalDateTime.now().toLocalDate().minusDays(3).atStartOfDay();
		List<UserLicense> userLicenseList = userLicenseRepository.findLicenseBetweenExpiryDate(startDate,currentDate);
		userLicenseList.forEach(userLicense -> {
			emailList.add(userLicense.getUser().getEmail());
			String expiryDate = userLicense.getLicense().getExpiryDate().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"));
			messageQueueService.sendMail(userLicense.getUser().getEmail(), "license_expire_notification_template.html", appConfig.getNewUserType(),
					appConfig.getUserVerifyPath(), null, "Wenplan License Expiry", null, null, null, expiryDate);
		});
		log.info("License Expire Remainder Status sent Emails"+emailList);
	}

	@Override
	public MSAResponse findExpirydate(String billingCycle) {
		String userTimeZone = userRepository.findTimeZoneByUserId(UserInfoContext.getUserThreadLocal());
		LocalDate expiryDate = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(LocalDateTime.now(), userTimeZone).toLocalDate();
		if (Constants.ANNUALLY.equalsIgnoreCase(billingCycle) || Constants.MONTHLY.equalsIgnoreCase(billingCycle) ) {
			if(Constants.ANNUALLY.equalsIgnoreCase(billingCycle))
				expiryDate = LocalDate.now().plusYears(1);
			if(Constants.MONTHLY.equalsIgnoreCase(billingCycle))
				expiryDate = LocalDate.now().plusDays(30);
			Map<String,String > response = new HashMap<>();
			response.put("expiryDate", expiryDate.toString());
			return new MSAResponse(true, HttpStatus.OK, SUCS, response);
		}
		else
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Billing cycle not found", null);
	}


}