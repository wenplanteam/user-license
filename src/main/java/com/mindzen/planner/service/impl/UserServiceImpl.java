package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.DEL;
import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.FND;
import static com.mindzen.infra.api.response.ApiResponseConstants.NOT;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;
import static com.mindzen.infra.api.response.ApiResponseConstants.UPD;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.converter.UserConverter;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserInfoDTO;
import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.CompanyUsers;
import com.mindzen.planner.entity.CurrencyMaster;
import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.LicenseDistribution;
import com.mindzen.planner.entity.LicenseDistributionHistory;
import com.mindzen.planner.entity.LicenseHistory;
import com.mindzen.planner.entity.PeriodicMaster;
import com.mindzen.planner.entity.Plan;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.entity.UserLicense;
import com.mindzen.planner.entity.UserLicenseHistory;
import com.mindzen.planner.entity.UserPlanDetail;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.CompanyRepository;
import com.mindzen.planner.repository.CompanyUsersRepository;
import com.mindzen.planner.repository.CurrencyMasterRepository;
import com.mindzen.planner.repository.LicenceDistributionRepositpry;
import com.mindzen.planner.repository.LicenseDistributionHistoryRepository;
import com.mindzen.planner.repository.LicenseDistributionRepository;
import com.mindzen.planner.repository.LicenseHistoryRepository;
import com.mindzen.planner.repository.LicenseRepository;
import com.mindzen.planner.repository.PeriodicMasterRepository;
import com.mindzen.planner.repository.PlanRepository;
import com.mindzen.planner.repository.UserLicenseHistoryRepository;
import com.mindzen.planner.repository.UserLicenseRepository;
import com.mindzen.planner.repository.UserPlanDetailRepository;
import com.mindzen.planner.repository.UserRepository;
import com.mindzen.planner.service.UserService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.EncodeDecode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserServiceImpl implements UserService {

	UserRepository userRepository;
	PlanRepository planRepository;
	CompanyRepository companyRepository;
	LicenceDistributionRepositpry licenceDistributionRepositpry;
	UserPlanDetailRepository userPlanDetailRepository;
	UserConverter userConverter;
	MessageQueueService messageQueueService;
	ModelMapper modelMapper;
	AppConfig appConfig;
	CurrencyMasterRepository currencyMasterRepository;
	PeriodicMasterRepository periodicMasterRepository;
	LicenseRepository licenseRepository;
	LicenseHistoryRepository licenseHistoryRepository;
	UserLicenseRepository userLicenseRepository;
	LicenseDistributionRepository licenseDistributionRepository;
	LicenseDistributionHistoryRepository licenseDistributionHistoryRepository;
	UserLicenseHistoryRepository userLicenseHistoryRepository;
	CompanyUsersRepository companyUsersRepository;

	@Value("${forgotPasswordTime}")
	private String forgotPasswordTime;

	@Value("${newUserCreationEmail}")
	private String newUserCreationEmail;

	MSAResponse response = null;

	public UserServiceImpl(UserRepository userRepository, CompanyRepository companyRepository, ModelMapper modelMapper,
			PlanRepository planRepository, LicenceDistributionRepositpry licenceDistributionRepositpry,
			UserPlanDetailRepository userPlanDetailRepository, MessageQueueService messageQueueService, AppConfig appConfig,
			CurrencyMasterRepository currencyMasterRepository, PeriodicMasterRepository periodicMasterRepository,
			LicenseRepository licenseRepository, LicenseHistoryRepository licenseHistoryRepository,
			UserLicenseRepository userLicenseRepository, LicenseDistributionRepository licenseDistributionRepository,
			LicenseDistributionHistoryRepository licenseDistributionHistoryRepository, UserLicenseHistoryRepository userLicenseHistoryRepository,
			CompanyUsersRepository companyUsersRepository) {
		this.userRepository = userRepository;
		this.companyRepository = companyRepository;
		this.userConverter = new UserConverter();
		this.modelMapper = modelMapper;
		this.licenceDistributionRepositpry = licenceDistributionRepositpry;
		this.userPlanDetailRepository = userPlanDetailRepository;
		this.planRepository = planRepository;
		this.messageQueueService = messageQueueService;
		this.appConfig = appConfig;
		this.currencyMasterRepository = currencyMasterRepository;
		this.periodicMasterRepository = periodicMasterRepository;
		this.licenseRepository = licenseRepository;
		this.licenseHistoryRepository = licenseHistoryRepository;
		this.userLicenseRepository = userLicenseRepository;
		this.licenseDistributionRepository = licenseDistributionRepository;
		this.licenseDistributionHistoryRepository= licenseDistributionHistoryRepository;
		this.userLicenseHistoryRepository = userLicenseHistoryRepository;
		this.companyUsersRepository = companyUsersRepository;
	}

	@Override
	public MSAResponse users(Long userId, Boolean createdBy, Long projectId, Integer pageNo, Integer pageSize, Long created) {
		if(null == pageNo)
			pageNo = 1;
		if(null == pageSize)
			pageSize = 1000;
		PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
		MSAResponse msaResponse = null;
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		if(null != createdBy && createdBy.equals(true)) {
			Long userInfoId = UserInfoContext.getUserThreadLocal();
			List<BigInteger>  userIdList = companyUsersRepository.finduserIdbyCreatedBy(userInfoId);
			List<Long> userIds=  new ArrayList<>();
			userIdList.forEach(id -> {
				Long key = Long.valueOf(id.toString());
				userIds.add(key);
			});
			userIds.add(userInfoId);
			Page<User> userPage = userRepository.findAllUserByIds(userIds, pageRequest);
			List<User> users = userPage.getContent();
			pagination.setTotalRecords(userPage.getTotalElements());
			msaResponse = new MSAResponse
					(true, REC + FND, userConverter.userEntityTOuserDTOConvert(users,companyRepository,userLicenseRepository,
							userRepository,companyUsersRepository,created), pagination, HttpStatus.OK);
		}
		else if (null != userId) {
			User user = userRepository.getActiveUserById(userId);
			if (null != user) 
				msaResponse = new MSAResponse(true, HttpStatus.OK, REC + FND, 
						userConverter.userTOuserDTOConverter(user,companyRepository,userLicenseRepository,userRepository,
								companyUsersRepository,created));
			else 
				return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
		else {
			Page<User> userPage = userRepository.getActiveUsers(pageRequest);
			List<User> users = userPage.getContent();
			pagination.setTotalRecords(userPage.getTotalElements());
			msaResponse = new MSAResponse
					(true, REC + FND, userConverter.userEntityTOuserDTOConvert(users,companyRepository,userLicenseRepository,userRepository,companyUsersRepository,created), pagination, HttpStatus.OK);
		}
		return msaResponse;
	}

	@Override
	public MSAResponse createUser(UserDTO userDTO) {
		userDTO.setEmail(userDTO.getEmail().toLowerCase());
		String templateFileName = "verify_mail.html";
		Long createdBy =UserInfoContext.getUserThreadLocal();
		// every signup create new company only
		/*List<Company> companyList = companyRepository.findbyCompanyListName(userDTO.getCompanyName());
		if (companyList.isEmpty()) {
			CompanyDTO companyDTO = new CompanyDTO();
			companyDTO.setName(userDTO.getCompanyName());
			companyDTO.setContactNo(userDTO.getContactNo());
			companyDTO.setEmail(userDTO.getEmail());
			company = userConverter.newCompanyCreation(companyDTO);
			company = companyRepository.save(company);
		}
		else {
			// company = companyList.get(0); if company name changes if may affect for others so every signup new company only
			CompanyDTO companyDTO = new CompanyDTO();
			companyDTO.setName(userDTO.getCompanyName());
			companyDTO.setContactNo(userDTO.getContactNo());
			companyDTO.setEmail(userDTO.getEmail());
			company = userConverter.newCompanyCreation(companyDTO);
			company = companyRepository.save(company);

		}*/

		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setName(userDTO.getCompanyName());
		companyDTO.setContactNo(userDTO.getContactNo());
		companyDTO.setEmail(userDTO.getEmail());
		Company company = userConverter.newCompanyCreation(companyDTO);
		company = companyRepository.save(company);

		User userValidation = userRepository.findByEmail(userDTO.getEmail());
		if (null == userValidation) {
			User user = userConverter.userDTOTOuserEntityConvert(userDTO, company);
			user = userRepository.save(user);
			CompanyUsers companyUser  = companyUsersRepository.findByUserCompanyCreatedBy(user,company,createdBy);
			if (null == companyUser) {
				companyUser = new CompanyUsers();
				companyUser.setCompany(company);
				companyUser.setUser(user);
				companyUsersRepository.save(companyUser);
			}
			associateLicenseToUser(user);
			if (!user.isEmailVerified()) {
				sendMail(user.getEmail(),templateFileName,newUserCreationEmail,null,user.getFirstName(),null);
				sendMail(user.getEmail(),"signup_user_creation.html",null,null,user.getFirstName()
						," Welcome to WenPlan ");
			}
			return new MSAResponse(true, HttpStatus.OK, "A mail has been sent to your business email id.", null);
		} 
		else if (!userValidation.isActive()) {
			User user = userConverter.userUpdateConvert(userDTO, company,userValidation);
			user = userRepository.save(user);
			CompanyUsers companyUser  = companyUsersRepository.findByUserCompanyCreatedBy(user,company,createdBy);
			if (null == companyUser) {
				companyUser = new CompanyUsers();
				companyUser.setCompany(company);
				companyUser.setUser(user);
				companyUsersRepository.save(companyUser);
			}
			associateLicenseToUser(user);
			if (!userValidation.isEmailVerified()) {
				sendMail(user.getEmail(),templateFileName,newUserCreationEmail,null,user.getFirstName(),null);
				sendMail(user.getEmail(),"signup_user_creation.html",null,null,user.getFirstName()
						," Welcome to WenPlan ");
				return new MSAResponse(true, HttpStatus.OK, "A mail has been sent to your business email id.", null);
			}
			else
				return new MSAResponse(true, HttpStatus.OK, "Success Proceed to Login", null);
		} 
		else if(!userValidation.isEmailVerified()) {
			sendMail(userValidation.getEmail(), templateFileName, null ,null, null,null);
			associateLicenseToUser(userValidation);
			return new MSAResponse(true, HttpStatus.OK, "A mail has been sent to your business email id.", null);
		}
		else if(userValidation.isDeleted() == true) {
			User user = userConverter.userUpdateConvert(userDTO, company, userValidation);
			user.setDeleted(false);
			user = userRepository.save(user);
			CompanyUsers companyUser =companyUsersRepository.findByUserCompanyCreatedBy(user,company,createdBy);
			if (null == companyUser) {
				companyUser = new CompanyUsers();
				companyUser.setCompany(company);
				companyUser.setUser(user);
				companyUsersRepository.save(companyUser);
			}
			associateLicenseToUser(user);
			if (!user.isEmailVerified())
				sendMail(user.getEmail().toLowerCase(), templateFileName ,null ,null, user.getFirstName(),null);
			return new MSAResponse(true, HttpStatus.OK, "A mail has been sent to your business email id.", null);
		}
		else {
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Email Already Exist", "E-001", null);
		}
	}

	private void associateLicenseToUser(User user) {
		UserLicense userLicense =  userLicenseRepository.findByUserAndActive(user, true);
		if(null == userLicense) {
			Plan plan = findbyPlanId(1L);
			UserPlanDetail userPlan = createUserPlan(plan);
			License license = createLicense(userPlan);
			licenseAssociate(license, user);
		}
	}

	private UserLicense licenseAssociate(License license, User user) {
		LicenseDistribution licenseDistribution =  new LicenseDistribution();
		licenseDistribution.setLicense(license);
		licenseDistribution.setProvidedDate(LocalDateTime.now());
		licenseDistribution.setProvidedTo(user);
		licenseDistribution.setTransferStatus(Constants.SUCCESS);
		licenseDistribution = licenseDistributionRepository.save(licenseDistribution);
		LicenseDistributionHistory licenseDistributionHistory =  new LicenseDistributionHistory();
		licenseDistributionHistory.setLicense(license);
		licenseDistributionHistory.setLicenseDistribution(licenseDistribution);
		licenseDistributionHistory.setProvidedBy(user);
		licenseDistributionHistory.setProvidedDate(LocalDateTime.now());
		licenseDistributionHistory.setProvidedTo(user);
		licenseDistributionHistory.setStatus(licenseDistribution.getTransferStatus());
		licenseDistributionHistoryRepository.save(licenseDistributionHistory);
		UserLicense userLicense = new UserLicense();
		userLicense.setActive(true);
		userLicense.setActiveFrom(LocalDateTime.now());
		userLicense.setLicense(license);
		userLicense.setUser(user);
		userLicense = userLicenseRepository.save(userLicense);
		UserLicenseHistory userLicenseHistory = new UserLicenseHistory();
		userLicenseHistory.setActiveFrom(LocalDateTime.now());
		userLicenseHistory.setLicense(license);
		userLicenseHistory.setUserLicenseId(userLicense);
		userLicenseHistoryRepository.save(userLicenseHistory);
		return userLicense;
	}

	private void sendMail(String email, String templateFileName, String newUserCreationEmail, String mailBodyEmail, 
			String firstName ,String mailSubject) {
		if(null == mailSubject)
			mailSubject = "Please confirm your WenPlan Account";
		String receiptUrl = null;
		String expiryDate = null;
		messageQueueService.sendMail(email.toLowerCase(), templateFileName, appConfig.getNewUserType(),
				appConfig.getUserVerifyPath(),mailBodyEmail,mailSubject,
				newUserCreationEmail, firstName, receiptUrl, expiryDate);
	}

	public License createLicense(UserPlanDetail userPlanDetail) {
		License license = new License();
		String curMilSec = Long.toString(System.currentTimeMillis());
		license.setCode("Lic-Free-" +  curMilSec );
		license.setStartDate(LocalDateTime.now());
		license.setExpiryDate(LocalDateTime.now().plusDays(30));
		license.setUserPlanDetail(userPlanDetail);
		license = licenseRepository.save(license);
		LicenseHistory licenseHistory = new LicenseHistory();
		licenseHistory.setCode(license.getCode());
		licenseHistory.setExpiryDate(license.getExpiryDate());
		licenseHistory.setLicenseId(license);
		licenseHistory.setStartDate(license.getStartDate());
		licenseHistory.setUserPlanDetail(userPlanDetail);
		licenseHistoryRepository.save(licenseHistory);
		return license;
	}

	public LicenseDistribution findByLicenceDistributionId(Long id) {
		return licenceDistributionRepositpry.findById(id).get();
	}

	public Plan findbyPlanId(Long planId) {
		return planRepository.findById(planId).get();
	}

	public UserPlanDetail createUserPlan(Plan plan) {
		UserPlanDetail userPlanDetail  = new UserPlanDetail();
		userPlanDetail.setActive(true);
		userPlanDetail.setAmount(BigDecimal.valueOf(0));
		CurrencyMaster currencyMaster = currencyMasterRepository.findById(1L).get();
		userPlanDetail.setCurrencyMaster(currencyMaster);
		userPlanDetail.setLicensesNos(1L);
		PeriodicMaster periodicMaster = periodicMasterRepository.findById(1L).get();
		userPlanDetail.setPeriodicMaster(periodicMaster);
		userPlanDetail.setPlan(plan);
		userPlanDetail.setUsedLicense(1L);
		return userPlanDetailRepository.save(userPlanDetail);
	}

	@Override
	public MSAResponse changePassword(AuthenticationRequest authenticationRequest) {
		Long userId = UserInfoContext.getUserThreadLocal();
		Optional<User> userOptional = userRepository.findById(userId);
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			user.setPassword(authenticationRequest.getNewPassword());
			user.setLastPasswordResetDate(Date.from(Instant.now()));
			userRepository.save(user);
			response= new MSAResponse(true, HttpStatus.OK, "Password "+ UPD + SUCSFLY, null);
		}
		else {
			response = new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
		return response;
	}

	@Override
	public MSAResponse deleteUser(List<Long> usersId) {
		Long createdBy = UserInfoContext.getUserThreadLocal();
		if (!usersId.isEmpty()) {
			List<CompanyUsers> companyUserList = companyUsersRepository.findByUserIdAndCreatedBy(usersId,createdBy);
			companyUserList.forEach(companyUser-> {
				companyUsersRepository.deleteById(companyUser.getId());
			});
			return new MSAResponse(true, HttpStatus.OK, REC + DEL + SUCSFLY, null);
		}
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
	}

	@Override
	public MSAResponse loadByEmail(String email) {
		UserDTO userDTO = new UserDTO();
		User user = userRepository.findByEmail(email.toLowerCase());
		if (null != user) {
			modelMapper.map(user, userDTO);
			CompanyUsers companyUser = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, user.getCreatedBy());
			if (null != companyUser)
				userDTO.setCompanyName(companyUser.getCompany().getName());
			return new MSAResponse(true, HttpStatus.OK, REC + FND, userDTO);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
	}

	@Override
	public MSAResponse verifyEmail(String email, String source) {
		User user = userRepository.findByEmail(email.toLowerCase());
		if(null != user) {
			user.setEmailVerified(true);
			user = userRepository.save(user);
			UserDTO userDTO =  userConverter.verifyEmail(
					user,userLicenseRepository,userRepository, companyUsersRepository);
			return new MSAResponse(true, HttpStatus.OK, SUCS,userDTO);
		}
		else {
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, null);
		}
	}

	@Override
	public MSAResponse createUserByBasicInfo(List<UserDTO> userDTOs) {
		try {
			List<UserDTO> resultUserDTOs = new ArrayList<>();
			userDTOs.forEach(userDTO -> {
				UserDTO resultUserDTO = new UserDTO();
				userDTO.setEmail(userDTO.getEmail().toLowerCase());
				User user = userRepository.findByEmail(userDTO.getEmail());
				if (null == user) {
					user = new User();
					user.setEmail(userDTO.getEmail());
					user.setFirstName(userDTO.getFirstName());
					user.setLastName(userDTO.getLastName());
					user.setTimeZone(Constants.DEFAULT_TIME_ZONE);
					user.setEmailVerified(false);
					user.setAccountExpired(false);
					user.setCredentialsExpired(false);
					user.setRoleName("ADMIN");
					user = userRepository.save(user);
				}
				modelMapper.map(user, resultUserDTO);
				resultUserDTO.setProjectRole(userDTO.getProjectRole());
				Long companyUserId =  userDTO.getCompanyUserId();
				CompanyUsers companyUsers = companyUsersRepository.findById(companyUserId).get();
				resultUserDTO.setCompanyId(companyUsers.getCompany().getId());
				resultUserDTO.setCompanyName(companyUsers.getCompany().getName());
				resultUserDTOs.add(resultUserDTO);
			});
			return new MSAResponse(true, HttpStatus.OK, SUCS, resultUserDTOs);
		} catch (Exception e) {
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, "user company Id missing", "E-001", null);
		}
	}

	@Override
	public MSAResponse decode(String data) {
		String email = EncodeDecode.decode(data).toLowerCase();
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail(email);
		return new MSAResponse(true, HttpStatus.OK, SUCS, userDTO);
	}

	@Override
	public MSAResponse findProjectUserByProjectId(Long projectId) {
		List<Object[]> resultUsers = userRepository.findProjectUserByProjectId(projectId);
		if (null != resultUsers && !resultUsers.isEmpty()) {
			List<UserDTO> userDTOs = new ArrayList<>();
			for (Object[] objects : resultUsers) {
				UserDTO users = new UserDTO();
				users.setFirstName((String) objects[0]);
				users.setLastName((String) objects[1]);
				users.setEmail(((String) objects[2]).toLowerCase());
				users.setContactNo((String) objects[3]);
				users.setRoleName((String) objects[4]);
				users.setId(((BigInteger) objects[5]).longValue());
				String[] access = null;
				if (!"".equals(objects[6])) {
					access = ((String) objects[6]).split(",");
				}
				log.info("Access: "+access.toString());
				users.setLookaheadAccess(access);
				users.setProjectRole((String) objects[7]);
				Long createdBy = ((BigInteger) objects[9]).longValue();

				if(users.getId().equals(createdBy)) {
					Long created = userRepository.findCreatedByByUserId(users.getId());
					User user = userRepository.findById(users.getId()).get();
					CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, created);
					if(null != companyUsers && null != companyUsers.getCompany()) {
						Company companys =  companyUsers.getCompany();
						users.setCompanyId(companys.getId());
						users.setCompanyName(companys.getName());
						users.setCompanyUserId(companyUsers.getId());
					}
				}
				else {
					User user = userRepository.findById(users.getId()).get();
					CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, createdBy);
					if(null != companyUsers && null != companyUsers.getCompany()) {
						Company companys =  companyUsers.getCompany();
						users.setCompanyId(companys.getId());
						users.setCompanyName(companys.getName());
						users.setCompanyUserId(companyUsers.getId());
					}
				}
				userDTOs.add(users);
			}

			return new MSAResponse(true, HttpStatus.OK, REC + FND + SUCSFLY, userDTOs);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
	}

	@Override
	public MSAResponse deleteUserByEmail(String email) {
		User userEmail = userRepository.findByEmail(email.toLowerCase());
		if (null != userEmail) {
			userRepository.delete(userEmail);
			return new MSAResponse(true, HttpStatus.OK, REC + DEL + SUCSFLY, null);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, null);
		}
	}

	@Override
	public MSAResponse findUsersByIds(List<Long> usersIds) {
		List<User> userList = null;
		List<UserDTO> userDTOs = null;
		if(usersIds != null) {
			userList = userRepository.findAllById(usersIds);
		}
		if(null != userList) {
			userDTOs = userConverter.userEntityTOuserDTOConvert(userList,companyRepository, userLicenseRepository, 
					userRepository, companyUsersRepository, null);
			return new MSAResponse(true, HttpStatus.OK, REC + FND, userDTOs);
		}
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, null);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserService#newPasswordUpdate(com.mindzen.planner.dto.AuthenticationRequest)
	 */
	@Override
	public MSAResponse forgotPassword(AuthenticationRequest authenticationRequest) {
		authenticationRequest.setEmail(authenticationRequest.getEmail().toLowerCase());
		User user = userRepository.findByEmail(authenticationRequest.getEmail());
		String message = null;
		UserDTO userDTO = null;
		if( null != user) {
			if(null != authenticationRequest.getNewPassword() && !authenticationRequest.getNewPassword().isEmpty()) {
				user.setPassword(authenticationRequest.getNewPassword());
				message =  "Password Updated successfully";
			}
			else {
				String templateFileName = "forgot_password_mail.html";
				String receiptUrl = null;
				String expiryDate = null;
				messageQueueService.sendMail(authenticationRequest.getEmail(), templateFileName,
						appConfig.getForgotPasswordType(), appConfig.getForgotPasswordPath(), null,
						"Wenplan User Password Reset" ,null, templateFileName ,receiptUrl,expiryDate);
				message =  "A mail has been sent to your business email id.";
			}
			user.setLastPasswordResetDate(Date.from(Instant.now()));
			user = userRepository.save(user);
			userDTO = userConverter.userTOuserDTOConverter(user, companyRepository, userLicenseRepository, userRepository, companyUsersRepository, null);
			response= new MSAResponse(true, HttpStatus.OK, message, userDTO);
		}
		else
			response= new MSAResponse(false, HttpStatus.NOT_FOUND, "EMAIL NOT EXIST" , null);
		return response;
	}

	@Override
	public MSAResponse findUsersByEmail(String email) {
		List<User> usersList = userRepository.findByEmailContainingIgnoreCase(email.toLowerCase());
		List<UserDTO> userDTOList = userConverter.userEntityTOuserDTOConvert(usersList, companyRepository, 
				userLicenseRepository, userRepository, companyUsersRepository, null);
		if(null != userDTOList && !userDTOList.isEmpty())
			return new MSAResponse(true, HttpStatus.OK, REC + FND, userDTOList);
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
	}

	@Override
	public MSAResponse updateUser(UserDTO userDTO) {
		Optional<User> userOptional = userRepository.findById(userDTO.getId()); 
		if(userOptional.isPresent()) {
			try {
				User user = userOptional.get();
				if(null !=userDTO.getFirstName())
					user.setFirstName(userDTO.getFirstName());
				if(null != userDTO.getLastName())
					user.setLastName(userDTO.getLastName());
				if(null != userDTO.getContactNo())
					user.setContactNo(userDTO.getContactNo());
				if(null != userDTO.getTimeZone())
					user.setTimeZone(userDTO.getTimeZone());
				userRepository.save(user);
				if(null != userDTO.getCompanyUserId()) {
					CompanyUsers companyUsers = companyUsersRepository.findById(userDTO.getCompanyUserId()).get();
					Company company = null;
					if(null !=  userDTO.getCompanyId())
						company = companyRepository.findById(userDTO.getCompanyId()).get();
					else {
						company =  new Company();
						if(null != userDTO.getCompanyName())
							company.setName(userDTO.getCompanyName());
						else
							return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Companyname not found",  "E-001", null);
						company = companyRepository.save(company);
					}
					companyUsers.setCompany(company);
					companyUsersRepository.save(companyUsers);
				}
				return new MSAResponse(true, HttpStatus.OK, UPD + SUCSFLY,
						userConverter.userTOuserDTOConverter(user,companyRepository,userLicenseRepository,userRepository, companyUsersRepository, null));
			}
			catch (Exception e) {
				return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Company user info id not found",  "E-001", null);
			}
		}
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
	}

	@Override
	public MSAResponse createBasicUser(UserDTO userDTO) {
		userDTO.setEmail(userDTO.getEmail().toLowerCase());
		Long userId = UserInfoContext.getUserThreadLocal();
		User user = userRepository.findByEmail(userDTO.getEmail());
		String msg =Constants.USER+" "+Constants.CREATE+"d "+SUCSFLY;
		Company company = null ;
		if(null != userDTO.getCompanyId()) {
			//company = companyRepository.findByIdAndCreatedBy(userDTO.getCompanyId(),userId);
			Optional<Company> companyOptional = companyRepository.findById(userDTO.getCompanyId());
			if(companyOptional.isPresent())
				company = companyOptional.get();
		}
		if(null == company && null != userDTO.getCompanyName()) {
			company = new Company();
			company.setName(userDTO.getCompanyName());
			company.setIsActive(true);
			company = companyRepository.save(company);
		}
		if (null == user) {
			user = new User();
			user.setEmail(userDTO.getEmail());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setActive(false);
			user.setTimeZone(Constants.DEFAULT_TIME_ZONE);
			user.setEmailVerified(false);
			user.setAccountExpired(false);
			user.setCredentialsExpired(false);
			user.setRoleName("ADMIN");
			user = userRepository.save(user);
			if(null == user.getFirstName())
				user.setFirstName("");
		}
		CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, userId);
		if(null !=companyUsers) {
			log.info("User already in wenplan");
			msg = "User already available in userList";
		}
		else {
			CompanyUsers companyUser =companyUsersRepository.findByUserAndCreatedBy(user.getId(), company.getId());
			if (null == companyUser) {
				companyUser = new CompanyUsers();
				companyUser.setCompany(company);
				companyUser.setUser(user);
				companyUsersRepository.save(companyUser);
			}
		}
		if(user.isDeleted() == true) {
			user.setDeleted(false);
			user.setAccountExpired(false);
			user.setCredentialsExpired(false);
			userRepository.save(user);
		}
		return new MSAResponse(true, HttpStatus.OK, msg , null);
	}

	@Override
	public MSAResponse getAllUserInformations(Integer pageNo, Integer pageSize) {
		List<UserInfoDTO> userInfoDTOs = new ArrayList<>();
		if (null == pageNo) 
			pageNo = 1;
		if (null ==pageSize)
			pageSize = 10;
		PageRequest pageRequest =  PageRequest.of(pageNo-1, pageSize,Direction.DESC ,"lastLoggedIn");
		User userInfo = userRepository.findById(UserInfoContext.getUserThreadLocal()).get();
		String zoneValue = userInfo.getTimeZone();
		Page<User> usersPage = userRepository.findAll(pageRequest);
		usersPage.getContent().forEach(user -> {
			UserInfoDTO userInfoDTO = new UserInfoDTO();
			userInfoDTO.setName(user.getFirstName() + " " +user.getLastName());
			userInfoDTO.setEmail(user.getEmail().toLowerCase());
			if(null == user.getLastLoggedIn())
				userInfoDTO.setLastLoggedIn(null);
			else {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm a /MMM dd,yyyy");
				LocalDateTime ldt = convertToLocalDateTimeViaSqlTimestamp(user.getLastLoggedIn());
				LocalDateTime latsLoginTime =  DateUtil.utcZoneDateTimeToZoneDateTimeConvert(ldt,zoneValue);
				userInfoDTO.setLastLoggedIn(latsLoginTime.format(formatter));
			}
			userInfoDTO.setContactNo(user.getContactNo());
			Long usersInvited = companyUsersRepository.countByCreatedBy(user.getId());
			Long companyCreated = companyRepository.countByCreatedBy(user.getId());
			Long projectsCreated = userRepository.createProjectCountsByCreatedBy(user.getId());
			Long projectsInvited = userRepository.inviteProjectCountsByCreatedBy(user.getId());
			if( user.getCreatedBy() == 1 )
				companyCreated +=1;
			userInfoDTO.setUsersInvited(usersInvited);
			userInfoDTO.setProjectsCreated(projectsCreated);
			userInfoDTO.setCompaniesAdded(companyCreated);
			userInfoDTO.setProjectsInvited(projectsInvited);
			userInfoDTOs.add(userInfoDTO);
		});
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		pagination.setTotalRecords(usersPage.getTotalElements());
		return new MSAResponse(true, SUCS, userInfoDTOs, pagination, HttpStatus.OK);
	}
	private LocalDateTime convertToLocalDateTimeViaSqlTimestamp(Date dateToConvert) {
		return new java.sql.Timestamp(dateToConvert.getTime()).toLocalDateTime();
	}
}
