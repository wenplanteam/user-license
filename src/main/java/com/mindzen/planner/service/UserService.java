package com.mindzen.planner.service;

import java.util.List;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.UserDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserService.
 *
 * @author naveenkumar Boopathi
 */
public interface UserService {

	/**
	 * check the condition based load the user data into user DTO.
	 *
	 * @param userId the user id
	 * @param createdBy 
	 * @param projectId 
	 * @param pageSize 
	 * @param pageNo 
	 * @param created 
	 * @return the api response
	 */
	public MSAResponse users(Long userId, Boolean createdBy, Long projectId, Integer pageNo, Integer pageSize, Long created);

	/**
	 * get values from user dto to entity then save the value into db.
	 *
	 * @param userDTO the user DTO
	 * @return the api response
	 */
	public MSAResponse createUser(UserDTO userDTO);

	/**
	 * check the record availability in database then update it.
	 * @param authenticationRequest 
	 *
	 * @return the api response
	 */
	public MSAResponse changePassword(AuthenticationRequest authenticationRequest);

	/**
	 * check the record availability in database then delete the record [soft
	 * delete].
	 *
	 * @param userId the user id
	 * @return the api response
	 */
	public MSAResponse deleteUser(List<Long> userId);

	/**
	 * Load by email.
	 *
	 * @param email the email
	 * @return the api response
	 */
	public MSAResponse loadByEmail(String email);

	/**
	 * decode the encoded email and verify the user.
	 *
	 * @param email           the email
	 * @param fromProcessFlag
	 * @return the api response
	 * 
	 */
	public MSAResponse verifyEmail(String email, String source);

	/**
	 * Create a new user with basic information.
	 *
	 * @param userDTO the user DTO
	 * @return the api response
	 */
	public MSAResponse createUserByBasicInfo(List<UserDTO> userDTOs);

	/**
	 * 
	 * @param code
	 * @return
	 */
	public MSAResponse decode(String data);

	/**
	 * 
	 * @param ProjectId
	 * @param created 
	 * @return
	 */
	MSAResponse findProjectUserByProjectId(Long ProjectId);

	/**
	 * 
	 * @param email
	 * @return
	 */
	MSAResponse deleteUserByEmail(String email);

	/**
	 * @param usersIds
	 * @return
	 */
	public MSAResponse findUsersByIds(List<Long> usersIds);

	/**
	 * @param authenticationRequest
	 * @return
	 */
	public MSAResponse forgotPassword(AuthenticationRequest authenticationRequest);

	public MSAResponse findUsersByEmail(String email);

	public MSAResponse updateUser(UserDTO userDTO);

	public MSAResponse createBasicUser(UserDTO userDTO);

	public MSAResponse getAllUserInformations(Integer pageNo, Integer pageSize);

}
