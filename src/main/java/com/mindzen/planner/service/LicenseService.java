package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;

public interface LicenseService {

	/**
	 * user plan data's are saved and payment has initialized
	 */
	public MSAResponse buyLicense(UserPlanMasterDTO userPlanMasterDTO);

	/**
	 * get the response from service provider create licenses
	 */
	public MSAResponse licenseResponse(StripeDataDTO stripeData);

	/**  no param's needed
	 *   get bought licenses based on which user log in 
	 * @param pageSize 
	 * @param pageNo 
	 */
	public MSAResponse userLicense(Integer pageNo, Integer pageSize, String status);

	/**
	 *  get License code and existing user ,
	 *  Then provide license to User
	 */
	public MSAResponse provideLicense(LicenseDistributionDTO licenseDistributionDTO);

	/**
	 * get license from given license code
	 * and license validation and in activate the license 
	 */
	public MSAResponse revokeLicense(LicenseDistributionDTO licenseDistributionDTO);

	public MSAResponse userLicenseCount();

	public MSAResponse userLicenseAvailabilityCheck(String email);

	public MSAResponse findExpirydate(String billingCycle);

}
