package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.CurrencyMaster;

@Repository
public interface CurrencyMasterRepository extends JpaRepository<CurrencyMaster, Long> {

	
}
