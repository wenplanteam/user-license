package com.mindzen.planner.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.User;
import com.mindzen.planner.entity.UserPlanMaster;

@Repository
public interface UserPlanMasterRepository extends JpaRepository<UserPlanMaster, Long> {

	public UserPlanMaster findByTransactionId(String transactionId);

	public List<UserPlanMaster> findByPurchasedBy(User user);

	public List<UserPlanMaster> findByPurchasedByAndPaymentStatus(User user, String paymentStatus );
	
}
