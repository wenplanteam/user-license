package com.mindzen.planner.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.UserPlanDetail;
import com.mindzen.planner.entity.UserPlanMaster;

@Repository
public interface UserPlanDetailRepository extends JpaRepository<UserPlanDetail, Long> {

	@Query(value = " SELECT upd FROM UserPlanDetail upd join upd.userPlanMaster upm WHERE upm.id IN ( ?1 ) ")
	public List<UserPlanDetail> finAlldByUserPlanMasterId(List<Long> userPlanMasterDetailId);

	public List<UserPlanDetail> findByUserPlanMaster(UserPlanMaster userPlanMaster);
	
}
