package com.mindzen.planner.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mindzen.planner.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	@Query("from User u where u.email = ?1")
	public User findByEmail(String email);
	
	public User findByEmailIgnoreCase(String email);

	@Query(" SELECT u FROM User u where u.id  =?1 and u.isDeleted = false order by u.id desc ")
	public User getActiveUserById(Long userId);
	
	@Query(" SELECT u FROM User u where u.isDeleted = false order by u.id desc ")
	public Page<User> getActiveUsers(PageRequest pageRequest);

	
	@Query(nativeQuery = true, value = " select u.first_name, u.Last_name, u.email, u.contact_no, u.role_name,u.id, "
	+ " pu.access,pu.project_role,pu.project_company_id,pu.created_by from users u join project_users pu on u.id = pu.user_id join projects p on p.id = pu.project_id "
	+ " where pu.project_id=?1"
	//+ " and pu.is_active = true"
	+ " and pu.is_deleted = false ")
	public List<Object[]> findProjectUserByProjectId(Long projectId);

	@Query(" SELECT u.id FROM User u WHERE u.createdBy = ?1 AND u.isActive = ?2 order by u.id desc ")
	public List<Long> findByCreatedByAndIsActive(Long userId, boolean active);

	public List<User> findByEmailContainingIgnoreCase(String email);

//	public User findByCompanyId(Long id);

//	@Query(" select u FROM User u where u.isActive =true AND u.companyId IN ( ?1 ) ")
//	public List<User> findAllUsersByCompanyId(List<Long> comapnyIdList, boolean status);

 	@Query(nativeQuery = true, 
 			value = " select pu.id from project_users pu where pu.user_id = ?1 AND is_deleted = false ")
	public List<Long> findUserAvailabilityInProjectByUserId(Long id);

 	@Query(" SELECT u.id FROM User u WHERE u.createdBy = ?1 And u.isDeleted = false order by u.id desc ")
	public List<Long> findByCreatedBy(Long userId);

 	@Query(" SELECT u FROM User u where u.id IN ( ?1 ) order by u.id desc ")
	public Page<User> findAllUserByIds(List<Long> userIdList, PageRequest pageRequest);

 	@Query(" SELECT u.createdBy FROM User u where u.id  =?1 ")
	public Long findCreatedByByUserId(Long userId);

	public Long countByCreatedBy(Long id);

	@Query(nativeQuery =true,
			value = " select count(p) from projects p where p.created_by = ?1 ")
	public Long createProjectCountsByCreatedBy(Long id);

	@Query(nativeQuery = true,
			value = " select count(pu) from project_users pu where pu.user_id = ?1 and pu.created_by != ?1 ")
	public Long inviteProjectCountsByCreatedBy(Long id);

	@Query(nativeQuery = true,
			value = " select u.time_zone from users u where u.id = ?1 ")
	public String findTimeZoneByUserId(Long userId);

}
