package com.mindzen.planner.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.entity.UserLicense;

@Repository
public interface UserLicenseRepository extends JpaRepository<UserLicense, Long> {

	public UserLicense findByUserAndActive(User providedTo, boolean activeStatus);

	public UserLicense findByLicenseAndActive(License license, boolean activeStatus);

	public UserLicense findByLicenseAndUserAndActive(License license, User user, boolean activeStatus);

	public List<UserLicense> findByUserId(Long id);

	public List<UserLicense> findByUser(User user);

	public List<UserLicense> findByActive(boolean activeStatus);
	
	@Transactional
	@Modifying
	@Query(value = " update UserLicense u set u.active = false where u.id IN (?1) ")
	public void licenseStatusUpdate(List<Long> licenseIds);

	@Query(value = " SELECT u from UserLicense u JOIN u.license l where  l.expiryDate < now() and u.active = true ")
	public List<UserLicense> findExpiredLicenses();

	@Query(value = " SELECT u from UserLicense u JOIN u.license l where u.active = true and l.expiryDate between ?1 and ?2 ")
	public List<UserLicense> findLicenseBetweenExpiryDate(LocalDateTime startDate, LocalDateTime currentDate);

}
