package com.mindzen.planner.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.UserPlanDetail;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {

	public List<License> findByUserPlanDetail(UserPlanDetail userPlanDetail);

	public License findByCode(String licenseNo);

	@Query(value = " SELECT l FROM License l JOIN l.userPlanDetail upd WHERE upd.id IN( ?1 ) ")
	//public Page<License> findByUserPlanDetailAndPageRequest(List<Long> userPlanDetailIds, Pageable pageRequest);
	public List<License> findByUserPlanDetail(List<Long> userPlanDetailIds);
	

	@Query(value = " SELECT l FROM License l WHERE l.id IN ( ?1 ) ")
	public Page<License> findLicensesWithPagination(List<Long> licenseIds, Pageable pageRequest);

	@Query(value=" select l from License l where  l.expiryDate > now() AND  l.id IN ( ?1 ) ")
	public Page<License> findLicensesAndValidWithPagination(List<Long> licenseIds, PageRequest pageRequest);

	@Query(value=" select l from License l where  l.expiryDate < now() AND l.id IN ( ?1 ) ")
	public Page<License> findLicensesAndExpiredWithPagination(List<Long> licenseIds, PageRequest pageRequest);

	
}
