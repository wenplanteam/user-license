package com.mindzen.planner.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.CompanyUsers;
import com.mindzen.planner.entity.User;

@Repository
public interface CompanyUsersRepository extends JpaRepository<CompanyUsers, Long> {

	
	public List<CompanyUsers> findByUser(User user);
	
	public List<CompanyUsers> findByCompany(Company company);
	
	@Query(" select cu FROM CompanyUsers cu JOIN cu.company c where c.id IN ( ?1 ) ")
	public List<CompanyUsers> findAllUsersByCompanyId(List<Long> comapnyIdList);

	@Query(nativeQuery = true ,value = " select cu from company_users cu where cu.user_id = ?1 and cu.company_id =(select id from company c where c.id = ?2 "
			+ "and  c.created_by = (select created_by from users u where u.id = ?1 ))")
	public CompanyUsers findByUserAndCreatedBy(Long userId, Long created);

	@Query(value = " select cu from CompanyUsers cu where cu.user = ?1 and  cu.createdBy = ?2 ")
	public CompanyUsers findCompanyUsersByuserAndCreatedby(User user, Long created);

	@Query(value = " select cu from CompanyUsers cu where cu.company In ( ?1 ) ")
	public List<CompanyUsers> findByCompanyIds(List<Long> companyIdList);

	@Query(nativeQuery = true ,value =" Select c from company_users c where c.user_id = ?1 and c.created_by = (select u.created_by from users u where u.id = ?1) ")
	public CompanyUsers findCompanyUserByuserId(Long userId);

	@Query(" SELECT cu.id FROM CompanyUsers cu WHERE cu.createdBy = ?1 order by cu.id desc ")
	public List<Long> findByCreatedBy(Long userInfoId);

	@Query(nativeQuery = true ,value = " select cu.user_id from company_users cu where cu.created_by = ?1 ")
	public List<BigInteger> finduserIdbyCreatedBy(Long userInfoId);

	@Query( " select cu from CompanyUsers cu where cu.company = ?1 and cu.user = ?2 ")
	public CompanyUsers findByCompanyIdAndUserId(Company company, User user);
	
	@Query( " select cu from CompanyUsers cu where cu.company = ?2 and cu.user = ?1 and cu.createdBy = ?3 ")
	public CompanyUsers findByUserCompanyCreatedBy(User user, Company company, Long createdBy);

	@Query(value = "select cu from CompanyUsers cu join cu.user u where u.id IN ( ?1 ) and cu.createdBy = ?2 " )
	public List<CompanyUsers> findByUserIdAndCreatedBy(List<Long> usersId, Long createdBy);

	public Long countByCreatedBy(Long id);

}
