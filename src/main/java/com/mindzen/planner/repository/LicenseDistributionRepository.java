package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.LicenseDistribution;

@Repository
public interface LicenseDistributionRepository extends JpaRepository<LicenseDistribution, Long> {

	public LicenseDistribution findByLicense(License license);

	
}
