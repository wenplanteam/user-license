package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.LicenseDistribution;

@Repository
public interface LicenceDistributionRepositpry extends JpaRepository<LicenseDistribution, Long>{

//	public LicenseDistribution findByLicenseToUserId(Long userId);

}
