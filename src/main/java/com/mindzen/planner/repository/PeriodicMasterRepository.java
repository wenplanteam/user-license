package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.PeriodicMaster;

@Repository
public interface PeriodicMasterRepository extends JpaRepository<PeriodicMaster, Long> {

	
}
