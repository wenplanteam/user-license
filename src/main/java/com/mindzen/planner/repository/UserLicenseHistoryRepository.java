package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.UserLicense;
import com.mindzen.planner.entity.UserLicenseHistory;

@Repository
public interface UserLicenseHistoryRepository extends JpaRepository<UserLicenseHistory, Long>{

	public UserLicenseHistory findByUserLicenseIdAndLicense(UserLicense userLicense, License license);

}
