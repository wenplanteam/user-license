package com.mindzen.planner.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mindzen.planner.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {
	
	@Query(" SELECT x FROM Company  x  where x.name =?1 AND x.isActive =true order by x.id desc ")
	public List<Company>  findbyCompanyListName(String company);

	@Query(" SELECT x FROM Company  x where x.isActive =true order by x.id desc ")
	public Page<Company> getActiveCompanies(PageRequest pageRequest);

	public List<Company> findByEmail(String email);

	@Query(" SELECT c.id FROM Company c WHERE c.createdBy = ?1 AND c.isActive = ?2 order by c.id desc ")
	public List<Long> findByCreatedByAndIsActive(Long userId, boolean active);

	@Query(nativeQuery = true, value = " SELECT pc.id from project_companies pc where pc.company_id = ?1  AND pc.is_active = true ")
	public List<Long> findProjectCompanyIdByCompanyId(Long id);
	
	@Query(" SELECT c FROM Company c where c.id IN ( ?1 ) and c.isActive =true order by c.id desc ")
	public Page<Company> findAllCompanyByIds(List<Long> companyIdList, PageRequest pageRequest);

	@Query(" select c from Company c where c.id = ?1 and c.createdBy = ?2 ")
	public Company findByIdAndCreatedBy(Long companyId, Long userId);

	@Query(nativeQuery = true, value = " SELECT pc.company_id from project_companies pc where pc.id = ?1 ")
	public Long findCompanyIdByProjectCompanyId(Long projectCompanyId);

	public Company findByNameAndCreatedBy(String name, Long userId);

	public Long countByCreatedBy(Long id);

}
