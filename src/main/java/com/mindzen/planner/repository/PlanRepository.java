package com.mindzen.planner.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Plan;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Long>{

	public Page<Plan> findByActiveAndPaid(boolean active, boolean paid,  Pageable pageRequest);

	public Page<Plan> findByActive(boolean active,  Pageable pageRequest);

	public Plan findByIdAndActive(Long id, boolean active);

}
