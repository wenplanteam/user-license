package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.License;
import com.mindzen.planner.entity.LicenseDistributionHistory;
import com.mindzen.planner.entity.User;

@Repository
public interface LicenseDistributionHistoryRepository extends JpaRepository<LicenseDistributionHistory, Long>{

	public LicenseDistributionHistory findByLicense(License license);

	public LicenseDistributionHistory findByLicenseAndProvidedTo(License license, User user);

	public LicenseDistributionHistory findByLicenseAndProvidedToAndStatus(License license, User user, String status);

}
