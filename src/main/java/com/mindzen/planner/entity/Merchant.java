package com.mindzen.planner.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="merchant")
public class Merchant extends BaseIdEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="name",nullable=true)
	private String name;

	@Column(name="code",nullable=true,unique =true)
	private String code;

	@Column(name="merchant_id",nullable=true,unique =true)
	private Long merchantId;

	@Column(name="is_active",nullable=true)
	private boolean active;


	// GETTERS & SETTERS FOR FIELDS
	public Merchant() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}



}
