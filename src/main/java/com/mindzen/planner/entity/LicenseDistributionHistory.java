package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="license_distribution_history")
public class LicenseDistributionHistory extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="provided_by")
	private User providedBy;

	@ManyToOne
	@JoinColumn(name="provided_to")
	private User providedTo;

	@Column(name="provided_date")
	private LocalDateTime providedDate;

	@Column(name="revoked_date")
	private LocalDateTime revokedDate;

	@Column(name="status")
	private String status;

	@OneToOne
	@JoinColumn(name="license_id")
	private License license;

	@ManyToOne
	@JoinColumn(name="license_distribution_id")
	private LicenseDistribution licenseDistribution;

	// GETTERS & SETTERS FOR FIELDS
	public LicenseDistributionHistory() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public User getProvidedBy() {
		return providedBy;
	}

	public void setProvidedBy(User providedBy) {
		this.providedBy = providedBy;
	}

	public User getProvidedTo() {
		return providedTo;
	}

	public void setProvidedTo(User providedTo) {
		this.providedTo = providedTo;
	}

	public LocalDateTime getProvidedDate() {
		return providedDate;
	}

	public void setProvidedDate(LocalDateTime providedDate) {
		this.providedDate = providedDate;
	}

	public LocalDateTime getRevokedDate() {
		return revokedDate;
	}

	public void setRevokedDate(LocalDateTime revokedDate) {
		this.revokedDate = revokedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public LicenseDistribution getLicenseDistribution() {
		return licenseDistribution;
	}

	public void setLicenseDistribution(LicenseDistribution licenseDistribution) {
		this.licenseDistribution = licenseDistribution;
	}

}
