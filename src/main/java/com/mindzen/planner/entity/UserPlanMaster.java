package com.mindzen.planner.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

@Entity
@Table(name="user_plan_master")
public class UserPlanMaster extends BaseIdEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Digits(integer=7, fraction=2)
	@Column(name="amount",nullable=true)
	private BigDecimal amount;

	@Column(name="transaction_id",unique=true,nullable=true)
	private String transactionId;

	@Column(name="transaction_date",nullable=true)
	private LocalDateTime transactionDate;

	@Column(name="mode_of_payment",nullable=true)
	private String modeOfPayment;

	@Column(name="payment_status",nullable=true)
	private String paymentStatus;

	@OneToOne
	@JoinColumn(name="purchased_by",nullable=true)
	private User purchasedBy;

	@Column(name="google_transaction_id",nullable=true,unique=true)
	private String googleTransactionId;

	@ManyToOne
	@JoinColumn(name="currency_id",nullable=true)
	private CurrencyMaster currencyId;

	@OneToOne
	@JoinColumn(name="reference_id")
	private UserPlanMaster referenceId;

	// GETTERS & SETTERS FOR FIELDS
	public UserPlanMaster() {
		super();
	}
	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public User getPurchasedBy() {
		return purchasedBy;
	}

	public void setPurchasedBy(User purchasedBy) {
		this.purchasedBy = purchasedBy;
	}

	public String getGoogleTransactionId() {
		return googleTransactionId;
	}

	public void setGoogleTransactionId(String googleTransactionId) {
		this.googleTransactionId = googleTransactionId;
	}

	public CurrencyMaster getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(CurrencyMaster currencyId) {
		this.currencyId = currencyId;
	}
	public UserPlanMaster getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(UserPlanMaster referenceId) {
		this.referenceId = referenceId;
	}

}
