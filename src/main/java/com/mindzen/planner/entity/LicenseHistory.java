package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="license_history")
public class LicenseHistory extends BaseIdEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="code",nullable=true)
	private String code;

	@Column(name="start_date",nullable=true)
	private LocalDateTime startDate;

	@Column(name="expiry_date",nullable=true)
	private LocalDateTime expiryDate;

	@ManyToOne
	@JoinColumn(name="user_plan_detail_id")
	private UserPlanDetail userPlanDetail;

	@ManyToOne
	@JoinColumn(name="license_id")
	private License licenseId;

	// GETTERS & SETTERS FOR FIELDS

	public LicenseHistory() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public UserPlanDetail getUserPlanDetail() {
		return userPlanDetail;
	}

	public void setUserPlanDetail(UserPlanDetail userPlanDetail) {
		this.userPlanDetail = userPlanDetail;
	}

	public License getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(License licenseId) {
		this.licenseId = licenseId;
	}

}
