/**
 * 
 */
package com.mindzen.planner.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.EqualsAndHashCode;

/**
 * @author Alexpandiyan Chokkan
 *
 */
@MappedSuperclass
@EqualsAndHashCode(callSuper=false)
public class BaseIdEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

}
