package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_license")
public class UserLicense  extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Column(name="active_from")
	private LocalDateTime activeFrom;

	@OneToOne
	@JoinColumn(name="license_id")
	private License license;

	@OneToOne
	@JoinColumn(name="user_id",nullable=true)
	private User user;
	
	@Column(name="is_active",nullable=true)
	private boolean active;

	// GETTERS & SETTERS FOR FIELDS
	public UserLicense() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public LocalDateTime getActiveFrom() {
		return activeFrom;
	}

	public void setActiveFrom(LocalDateTime activeFrom) {
		this.activeFrom = activeFrom;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	
}
