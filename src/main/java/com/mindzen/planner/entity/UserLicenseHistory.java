package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_license_history")
public class UserLicenseHistory  extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Column(name="active_from",nullable=true)
	private LocalDateTime activeFrom;

	@Column(name="active_to",nullable=true)
	private LocalDateTime activeTo;

	@OneToOne
	@JoinColumn(name="user_license_id")
	private UserLicense userLicenseId;

	@OneToOne
	@JoinColumn(name="license_id")
	private License license;

	// GETTERS & SETTERS FOR FIELDS
	public UserLicenseHistory() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public LocalDateTime getActiveFrom() {
		return activeFrom;
	}

	public void setActiveFrom(LocalDateTime activeFrom) {
		this.activeFrom = activeFrom;
	}

	public LocalDateTime getActiveTo() {
		return activeTo;
	}

	public void setActiveTo(LocalDateTime activeTo) {
		this.activeTo = activeTo;
	}

	public UserLicense getUserLicenseId() {
		return userLicenseId;
	}

	public void setUserLicenseId(UserLicense userLicenseId) {
		this.userLicenseId = userLicenseId;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}
	
	
}
