package com.mindzen.planner.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="currency_master")
public class CurrencyMaster extends BaseIdEntity implements Serializable 
{

	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false)
	private String     name         ;

	@Column(name="is_active", nullable=false)
	private boolean    active     ;

	// GETTERS & SETTERS FOR FIELDS

	public CurrencyMaster() {
		super();
	}

	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


}
