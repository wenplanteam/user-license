/*
 * Created on 2018-09-12 ( Time 17:07:17 )
 * Generated by Telosys Tools Generator ( version 3.0.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "Company"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="company" )
// Define named queries here
@NamedQueries ( {
	@NamedQuery ( name="Company.countAll", query="SELECT COUNT(x) FROM Company x" )
} )
public class Company extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	//----------------------------------------------------------------------
	// ENTITY DATA FIELDS 
	//----------------------------------------------------------------------    
	@Column(name="name", nullable=false)
	private String     name         ;

	@Column(name="contact_no")
	private String     contactNo    ;

	@Column(name="address")
	private String     address      ;

	@Column(name="email")
	private String     email        ;

	@Column(name="contact_first_name")
	private String     contactFirstName ;

	@Column(name="contact_last_name")
	private String     contactLastName ;

	@Column(name="is_active", nullable=false)
	private boolean    isActive     ;

	//----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	//----------------------------------------------------------------------

	//----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	//----------------------------------------------------------------------
	public Company()
	{
		super();
	}


	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	//----------------------------------------------------------------------
	//--- DATABASE MAPPING : id (  ) 
	public Long getId()
	{
		if(null == id)
			return 0L;
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	//--- DATABASE MAPPING : createdBy (  ) 
	public Long getCreatedBy()
	{
		return createdBy;
	}

	//--- DATABASE MAPPING : createdOn (  ) 
	public LocalDateTime getCreatedOn()
	{
		return createdOn;
	}

	//--- DATABASE MAPPING : modifiedBy (  ) 
	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	//--- DATABASE MAPPING : modifiedOn (  ) 
	public LocalDateTime getModifiedOn()
	{
		return modifiedOn;
	}

	//--- DATABASE MAPPING : name (  ) 
	public void setName( String name )
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}

	//--- DATABASE MAPPING : contactNo (  ) 
	public void setContactNo( String contactNo )
	{
		this.contactNo = contactNo;
	}
	public String getContactNo()
	{
		return this.contactNo;
	}

	//--- DATABASE MAPPING : address (  ) 
	public void setAddress( String address )
	{
		this.address = address;
	}
	public String getAddress()
	{
		return this.address;
	}

	//--- DATABASE MAPPING : email (  ) 
	public void setEmail( String email )
	{
		this.email = email;
	}
	public String getEmail()
	{
		return this.email;
	}

	//--- DATABASE MAPPING : contactFirstName (  ) 
	public void setContactFirstName( String contactFirstName )
	{
		this.contactFirstName = contactFirstName;
	}
	public String getContactFirstName()
	{
		return this.contactFirstName;
	}

	//--- DATABASE MAPPING : contactLastName (  ) 
	public void setContactLastName( String contactLastName )
	{
		this.contactLastName = contactLastName;
	}
	public String getContactLastName()
	{
		return this.contactLastName;
	}

	//--- DATABASE MAPPING : isActive (  ) 
	public void setIsActive( boolean isActive )
	{
		this.isActive = isActive;
	}
	public boolean isIsActive()
	{
		return this.isActive;
	}


	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	//----------------------------------------------------------------------

	//----------------------------------------------------------------------
	// toString METHOD
	//----------------------------------------------------------------------
	public String toString() { 
		StringBuffer sb = new StringBuffer(); 
		sb.append("["); 
		sb.append("]:"); 
		sb.append(name);
		sb.append("|");
		sb.append(contactNo);
		sb.append("|");
		sb.append(address);
		sb.append("|");
		sb.append(email);
		sb.append("|");
		sb.append(contactFirstName);
		sb.append("|");
		sb.append(contactLastName);
		sb.append("|");
		sb.append(isActive);
		return sb.toString(); 
	} 

}