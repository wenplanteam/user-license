package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="company_users" )
// Define named queries here
@NamedQueries ( {
	@NamedQuery ( name="CompanyUsers.countAll", query="SELECT COUNT(x) FROM CompanyUsers x" )
} )
public class CompanyUsers  extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	public CompanyUsers()
	{
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}

	public Long getCreatedBy()
	{
		return createdBy;
	}

	public LocalDateTime getCreatedOn()
	{
		return createdOn;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public LocalDateTime getModifiedOn()
	{
		return modifiedOn;
	}
}
