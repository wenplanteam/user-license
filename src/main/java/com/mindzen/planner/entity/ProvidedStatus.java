package com.mindzen.planner.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="provided_status")
public class ProvidedStatus extends BaseIdEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Column(name="status",nullable=true)
	private String status;

	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	//----------------------------------------------------------------------
	//--- DATABASE MAPPING : id (  ) 
	public Long getId()
	{
		return id;
	}

	public Long setId(Long id)
	{
		return this.id = id;
	}
}
