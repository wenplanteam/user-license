/**
 * 
 */
package com.mindzen.planner.config;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Alexpandiyan Chokkan
 *
 * 12-Oct-2018
 */
@Slf4j
public class ConnectionFactory {

	private static HikariDataSource dataSource;

	private ConnectionFactory() {
	}

	public static Connection getConnection() {
		String database="wenplan";
		String username="postgres";
		String password="postGreSql";
		Connection connection = null;

		synchronized(ConnectionFactory.class) {
			if(null == dataSource) {
				String host = "postgres";
				connection = getDataSourceConnection(host, database, username, password);
				if(null == connection) {
					host = "192.168.1.131";
					connection = getDataSourceConnection(host, database, username, password);
				}
				if(null == connection) {
					host = "localhost";
					connection = getDataSourceConnection(host, database, username, password);
				}
			}
			else
				connection = getNewDatabaseConnection(dataSource);

		}
		return connection;
	}

	private static Connection getDataSourceConnection(String host, String database, String username, String password) {
		dataSource = new HikariDataSource();
		dataSource.setJdbcUrl("jdbc:postgresql://"+host+":5432/"+database);
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		return getNewDatabaseConnection(dataSource);
	}

	private static Connection getNewDatabaseConnection(HikariDataSource dataSource) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			log.debug("Logger db connected successfully.");
		} catch (SQLException e) {
			log.error("Logger db connection failed. SQL ErrorCode: "+e.getSQLState()+".  Msg: "+e.getMessage());
		}
		return connection;
	}

}