/**
 * 
 */
package com.mindzen.planner.config.audit;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.mindzen.planner.config.UserInfoContext;

/**
 * @author Alexpandiyan Chokkan
 *
 */
@Component
public class AuditorAwareImpl implements AuditorAware<Long> {

	@Autowired
	HttpSession httpSession;
	
	/* (non-Javadoc)
	 * @see org.springframework.data.domain.AuditorAware#getCurrentAuditor()
	 */
	@Override
	public Optional<Long> getCurrentAuditor() {
		return Optional.ofNullable(UserInfoContext.getUserThreadLocal());
	}

}
