/**
 * 
 */
package com.mindzen.planner.config.audit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Alexpandiyan Chokkan
 *
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef="auditorAware")
public class AuditJpaConfig {

	@Bean
	public AuditorAwareImpl auditorAware() {
		return new AuditorAwareImpl();
	}
	
}
