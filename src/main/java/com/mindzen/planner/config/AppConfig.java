/**
 * 
 */
package com.mindzen.planner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alexpandiyan Chokkan
 *
 * 25-Sep-2018
 */
@Configuration
public class AppConfig {

	@Value("${spring.mail.user.verify.url.host-port}")
	private String userVerifyHostAndPort;

	@Value("${spring.mail.user.verify.url.path}")
	private String userVerifyPath;
	
	@Value("${spring.mail.user.type.new}")
	private String newUserType;

	@Value("${spring.mail.user.type.shared}")
	private String sharedUserType;
	
	@Value("${spring.mail.user.type.forgot}")
	private String forgotPasswordType;
	
	@Value("${spring.mail.user.verify.url.forgotPassword}")
	private String forgotPasswordPath;

	public String getUserVerifyHostAndPort() {
		return userVerifyHostAndPort;
	}

	public void setUserVerifyHostAndPort(String userVerifyHostAndPort) {
		this.userVerifyHostAndPort = userVerifyHostAndPort;
	}

	public String getUserVerifyPath() {
		return userVerifyPath;
	}

	public void setUserVerifyPath(String userVerifyPath) {
		this.userVerifyPath = userVerifyPath;
	}

	public String getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(String newUserType) {
		this.newUserType = newUserType;
	}

	public String getSharedUserType() {
		return sharedUserType;
	}

	public void setSharedUserType(String sharedUserType) {
		this.sharedUserType = sharedUserType;
	}

	public String getForgotPasswordType() {
		return forgotPasswordType;
	}

	public void setForgotPasswordType(String forgotPasswordType) {
		this.forgotPasswordType = forgotPasswordType;
	}

	public String getForgotPasswordPath() {
		return forgotPasswordPath;
	}

	public void setForgotPasswordPath(String forgotPasswordPath) {
		this.forgotPasswordPath = forgotPasswordPath;
	}
	
	
	
}
