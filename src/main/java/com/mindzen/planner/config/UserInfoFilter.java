package com.mindzen.planner.config;


import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**@author Bharath Telukuntla*/
@Slf4j

/**
 * Gets the multitenant data source context.
 *
 * @return the multitenant data source context
 */
@Getter

/**
 * Sets the multitenant data source context.
 *
 * @param multitenantDataSourceContext the new multitenant data source context
 */
@Setter
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class UserInfoFilter extends GenericFilterBean {

	/** The Constant TENANT_HEADER_NAME. */
	private static final String TENANT_HEADER_NAME = "tenantId";

	/**
	 * The <code>doFilter</code> method of the Filter is called by the container
	 * each time a request/response pair is passed through the chain due to a
	 * client request for a resource at the end of the chain. The FilterChain
	 * passed in to this method allows the Filter to pass on the request and
	 * response to the next entity in the chain.
	 * @param request  The request to process
	 * @param response The response associated with the request
	 * @param chain    Provides access to the next filter in the chain for this
	 *                 filter to pass the request and response to for further
	 *                 processing
	 *
	 * @throws IOException if an I/O error occurs during this filter's
	 *                     processing of the request
	 * @throws ServletException if the processing fails for any other reason
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {

			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			if(request instanceof HttpServletRequest) {
				Long userId=null;
				try {
					httpServletRequest.getParameterMap().entrySet().forEach(entry -> log.info("Key: "+entry.getKey()+". Value: "+Arrays.toString(entry.getValue())));
					userId  = Long.valueOf(httpServletRequest.getHeader("userId"));
				} catch (Exception e) {
					log.error(e.getMessage());
					userId=1L;
				}
				log.info("User ID: "+userId);
				UserInfoContext.setUserThreadLocal(userId);
			}
			chain.doFilter(httpServletRequest, httpServletResponse);
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
