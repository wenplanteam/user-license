package com.mindzen.planner.config;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import lombok.extern.slf4j.Slf4j;

/**
 * Intercepting the API request to find out the time consumption for the service.
 * 
 * @author Alexpandiyan Chokkan
 *
 */
@Slf4j
public class RequestExecutionTimeInterceptor implements HandlerInterceptor{

	/* 
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		request.setAttribute("startTime", LocalDateTime.now());
		return true;
	}
	
	/* 
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		LocalDateTime endTime = LocalDateTime.now();
		String requestMethod = request.getMethod();
		LocalDateTime startTime = (LocalDateTime)request.getAttribute("startTime");
		request.setAttribute("endTime", endTime);
		if(!"OPTIONS".equalsIgnoreCase(requestMethod))
			log.debug(requestMethod+","
					+request.getServletPath()+","
					+startTime+","
					+endTime+","
					+Duration.between(startTime, endTime).toMillis());
	}
	
}