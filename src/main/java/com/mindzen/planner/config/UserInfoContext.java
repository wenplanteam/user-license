package com.mindzen.planner.config;

// TODO: Auto-generated Javadoc
/**
 * The Class UserInfoContext.
 *
 */
public class UserInfoContext {

	/**
	 * Instantiates a new user ids context.
	 */
	private UserInfoContext() {

	}

	/** The Constant USER_THREAD_LOCAL. */
	public static final ThreadLocal<Long> USER_THREAD_LOCAL = new ThreadLocal<>();

	/**
	 * Get the user id.
	 *
	 * @return the user id
	 */
	public static Long getUserThreadLocal() {
		return USER_THREAD_LOCAL.get();
	}

	/**
	 * Set the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public static void setUserThreadLocal(Long userId) {
		USER_THREAD_LOCAL.set(userId);
	}

}
