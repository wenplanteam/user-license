/**
 * 
 */
package com.mindzen.planner.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

/**
 * @author Alexpandiyan Chokkan
 *
 */
@MessagingGateway
public interface SpringIntegrationGateway {

	@Gateway(requestChannel="amqpVerifyUserMailChannel")
	void sendAmqpUserVerifyMessage(Message<String> message);

}
