package com.mindzen.planner.integration;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

@Configuration
public class SpringIntegrationConfiguration {

	@Bean
	public IntegrationFlow amqpUserVerifyMailOutboundAdapter(AmqpTemplate amqpTemplate) {
		return IntegrationFlows.from("amqpVerifyUserMailChannel")
				.handle(Amqp.outboundAdapter(amqpTemplate)
						.routingKey("wenplan-user-verify-mail"))
				.channel("log")
				.bridge()
				.get();
	}
	
	@Bean
	public IntegrationFlow print() {
		return IntegrationFlows.from("log")
				.log("Printing")
				.get();
	}

}
