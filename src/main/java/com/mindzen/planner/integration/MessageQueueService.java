package com.mindzen.planner.integration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.stringtemplate.v4.ST;

import com.mindzen.infra.mail.dto.MailDTO;
import com.mindzen.infra.mail.dto.MultipartInLineDTO;
import com.mindzen.infra.mail.dto.TemplateDTO;
import com.mindzen.infra.util.GsonUtil;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.util.EncodeDecode;
import com.mindzen.planner.util.PathUtil;

@Component
public class MessageQueueService {
	
	@Autowired
	private AppConfig appConfig;
	
	@Autowired
	private SpringIntegrationGateway integrationGateway;

	public void sendMail(String email, String templateName, String source, String urlPath, String emailId, 
			String mailSubject, String newUserCreationCCEmail, String firstName, String receiptUrl, String expiryDate) {
		
		MailDTO mailDTO = new MailDTO();
		mailDTO.setTo(email);
		mailDTO.setSubject(mailSubject);//("Wenplan User Verification");
		if(null != newUserCreationCCEmail) {
			mailDTO.setBcc(newUserCreationCCEmail);
		}
		TemplateDTO templateDTO = new TemplateDTO();
		templateDTO.setFileName(templateName);
		templateDTO.setFilePath(PathUtil.WENPLAN_MAIL_TEMPLATE_HOME);
		templateDTO.setContentType(MediaType.TEXT_HTML_VALUE);
		
		ST urlPathTemplate = new ST(urlPath, '$', '$');
		urlPathTemplate.add("source", source);
		urlPathTemplate.add("email", EncodeDecode.encode(email));

		Map<String, String> dataMap = new HashMap<>();
		dataMap.put("url", appConfig.getUserVerifyHostAndPort()+urlPathTemplate.render());
		if(null != emailId)
			dataMap.put("emailId", emailId);
		if(null != firstName)
			dataMap.put("firstName", firstName);
		if(null != receiptUrl) 
			dataMap.put("receiptUrl", receiptUrl);
		if(null != expiryDate)
			dataMap.put("licenseExpireDate", expiryDate);
		templateDTO.setDataMap(dataMap);
		templateDTO.setTemplate(true);

		mailDTO.setTemplate(templateDTO);

		MultipartInLineDTO demoMultipartInLineDTO = new MultipartInLineDTO();
		demoMultipartInLineDTO.setFileName("logo.png");
		demoMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		demoMultipartInLineDTO.setFileContentId("logo");
		demoMultipartInLineDTO.setFileContentType(MediaType.IMAGE_JPEG_VALUE);

		MultipartInLineDTO facebookMultipartInLineDTO = new MultipartInLineDTO();
		facebookMultipartInLineDTO.setFileName("facebook.png");
		facebookMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		facebookMultipartInLineDTO.setFileContentId("facebook");
		facebookMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO linkedinMultipartInLineDTO = new MultipartInLineDTO();
		linkedinMultipartInLineDTO.setFileName("linkedin.png");
		linkedinMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		linkedinMultipartInLineDTO.setFileContentId("linkedin");
		linkedinMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO twitterMultipartInLineDTO = new MultipartInLineDTO();
		twitterMultipartInLineDTO.setFileName("twitter.png");
		twitterMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		twitterMultipartInLineDTO.setFileContentId("twitter");
		twitterMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		mailDTO.setMultipartInlines(Arrays.asList(demoMultipartInLineDTO, facebookMultipartInLineDTO, linkedinMultipartInLineDTO, twitterMultipartInLineDTO));
		
		Message<String> message = MessageBuilder
				.withPayload(GsonUtil.toJsonStringFromObject(mailDTO))
				.setHeader("userId", UserInfoContext.getUserThreadLocal())
				.build();

		integrationGateway.sendAmqpUserVerifyMessage(message);
	}

}
