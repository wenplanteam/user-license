package com.mindzen.planner.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;
import com.mindzen.planner.service.LicenseService;

/**
 * @author Naveenkumar Boopathi
 *
 */
@RestController
public class LicenseController {

	@Resource
	LicenseService licenseService;
	
	
	/**
	 * get information from user plan master and payment has initiated
	 */
	@PostMapping("/buyLicense")
	public MSAResponse buyLicense(@RequestBody UserPlanMasterDTO userPlanMasterDTO) {
		return licenseService.buyLicense(userPlanMasterDTO);
	}
	
	/**
	 * get transaction id from service provider and create license
	 */
	@PostMapping("/charge")
	public MSAResponse getLicenseResponse(@RequestBody StripeDataDTO stripeData){
		return licenseService.licenseResponse(stripeData);
	}
	
	
	/**
	 * get bought licenses based on which user log in 
	 */
	@GetMapping("/licenses")
	public MSAResponse userLicenses(
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "status") String status) {
		return licenseService.userLicense(pageNo,pageSize,status);
	}
	
	@GetMapping("licenseCount")
	public MSAResponse userLicenseCount() {
		return licenseService.userLicenseCount();
	}
	
	/**
	 * provide license to existing user
	 */
	@PostMapping("/provideLicense")
	public MSAResponse provideLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO) {
		return licenseService.provideLicense(licenseDistributionDTO);
	}
	
	/**
	 * revoke license from existing user who have license based on license code
	 */
	@PostMapping("/revokeLicense")
	public MSAResponse revokeLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO) {
		return licenseService.revokeLicense(licenseDistributionDTO);
	}
	
	@GetMapping("/userLicenseAvailability")
	public MSAResponse userLicense(@RequestParam(required = true, value = "email") String email) {
		return licenseService.userLicenseAvailabilityCheck(email);
	}
	
	@GetMapping("/expirydate")
	public MSAResponse findExpirydate(@RequestParam(required = false, value = "billingCycle") String billingCycle) {
		return licenseService.findExpirydate(billingCycle);
	}

} 
