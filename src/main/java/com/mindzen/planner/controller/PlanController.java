package com.mindzen.planner.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.service.PlanService;

@RestController
public class PlanController {

	@Resource
	PlanService planService;
	
	@GetMapping("/plans")
	public MSAResponse loadPlans(@RequestParam(required = false ,value = "paid") boolean paid,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false ,value = "id") Long id) {
		return planService.getPlans(paid, pageNo, pageSize, id);
	}
	
	@PostMapping("/plan")
	public MSAResponse createPlan(@RequestBody PlanDTO planDTO) {
		return planService.createPlan(planDTO);
  	}
	
	@PutMapping("/plan")
	public MSAResponse updatePlan(@RequestBody PlanDTO planDTO) {
		return planService.updatePlan(planDTO);
  	}
	
	@DeleteMapping("/plan")
	public MSAResponse deletePlan(@RequestBody List<Long> planId) {
		return planService.deletePlan(planId);
		
	}
}
