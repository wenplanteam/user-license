package com.mindzen.planner.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.service.CompanyService;

/**
 * @author naveenkumar Boopathi
 *
 */
@RestController
public class CompanyController {

	@Resource
	CompanyService companyService;
	
	/**
	 * Load All company / load company based on company id
	 * @param id
	 * @return
	 */
	@GetMapping("/company")
	public MSAResponse companyList(@RequestParam(required = false ,value = "id") Long id,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize) {
		return companyService.companyList(id,createdBy,pageNo,pageSize);
	} 
	
	/**
	 * Insert and update company informations
	 * @param companyDTO
	 * @return
	 */
	@PostMapping("/company")
	public MSAResponse insertCompany(@RequestBody CompanyDTO companyDTO) {
		return companyService.insertCompany(companyDTO);
	}
	
	@PutMapping("/company")
	public MSAResponse updateCompany(@RequestBody CompanyDTO companyDTO) {
		return companyService.updateCompany(companyDTO);
	}
	
	
	/**
	 * In-Activate the company 
	 * @param companyId
	 * @return
	 */
	@DeleteMapping("/company")
	public MSAResponse inActivateCompany(@RequestBody List<Long> companyId) {
		return companyService.inActivateCompany(companyId);
	}
	
	
}
