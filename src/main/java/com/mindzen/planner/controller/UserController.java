package com.mindzen.planner.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.service.UserService;


/**
 * @author naveenkumar Boopathi
 *
 */
@RestController
public class UserController {

	@Resource
	UserService userService;


	/**
	 * case 1: List the all User
	 * case 2: List the user based on user Id
	 */
	@GetMapping("/users")
	public MSAResponse loadUsers(
			@RequestParam(required = false, value = "userId") Long userId,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false ,value = "projectId") Long projectId,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = true, value = "created") Long created) {
		return userService.users(userId,createdBy,projectId,pageNo,pageSize,created);
	}

	/**
	 * User creation 
	 */
	@PostMapping("/user")
	public MSAResponse createUser(@RequestBody UserDTO userDTO) {
		return userService.createUser(userDTO);
	}

	/**
	 * User update
	 */
	@PostMapping("/changePassword")
	public MSAResponse changePassword(@RequestBody AuthenticationRequest authenticationRequest) {
		return userService.changePassword(authenticationRequest);
	}

	/**
	 * user delete [soft delete]
	 */
	@DeleteMapping("/user")
	public MSAResponse deleteUser(@RequestBody List<Long> userIds) {
		return userService.deleteUser(userIds);
	}

	/**
	 * @param email
	 * @return
	 */
	@GetMapping("/findUserByEmail")
	public MSAResponse loadByEmail(@RequestParam(required = true, value = "email") String email) {
		return userService.loadByEmail(email);
	}

	/**
	 *  verify the email address 
	 *  note : the email address was encoded format so decode then use the meail  
	 */
	@GetMapping("/verifyEmail")
	public MSAResponse verifyEmail(@RequestParam(required = true, value = "email") String email
			,@RequestParam(required = true,value = "source")String source) {
		return userService.verifyEmail(email, source);
	}

	/**
	 * create user by the Bacic information for project shared users
	 * @param userDTOs
	 * @return
	 */
	@PostMapping("/createUserByBasicInfo")
	public MSAResponse createUserByBasicInfo(@RequestBody List<UserDTO> userDTOs){
		return userService.createUserByBasicInfo(userDTOs);
	}

	/**
	 * encode the code to decode for email 
	 * @param data
	 * @return
	 */
	@GetMapping("/decode")
	public MSAResponse decode(@RequestParam(required = true, value = "data") String data) {
		return userService.decode(data);
	}

	/**
	 * find the project users by  using the project Id
	 * @param projectId
	 * @return
	 */
	@GetMapping("/projectUsers")
	public MSAResponse findProjectUserByProjectId(@RequestParam(required = true,value = "projectId") Long projectId ) {
		return userService.findProjectUserByProjectId(projectId);
	}

	/**
	 *  delete user using email (internal purpose only)
	 * @param email
	 * @return
	 */
	@DeleteMapping("/delete")
	public MSAResponse deleteUser(@RequestParam(required = true,value = "email") String email) {
		return userService.deleteUserByEmail(email);
	}

	/**
	 * get multiple users using list of id's 
	 * @param usersIds
	 * @return
	 */
	@PostMapping("/findUsersByIds")
	public MSAResponse findUsersByIds(@RequestBody List<Long> usersIds) {
		return userService.findUsersByIds(usersIds);
	}

	/**
	 * forget password for user 
	 * @param authenticationRequest
	 * @return
	 */
	@PostMapping("/forgotPassword")
	public MSAResponse forgotPassword(@RequestBody AuthenticationRequest authenticationRequest) {
		return userService.forgotPassword(authenticationRequest);
	}

	@GetMapping("/findUsersByEmail")
	public MSAResponse findUsersByEmail(@RequestParam(required=true, value = "email") String email) {
		return userService.findUsersByEmail(email);

	}

	@PutMapping("/user")
	public MSAResponse updateUser(@RequestBody UserDTO userDTO) {
		return userService.updateUser(userDTO);
	}

	@PostMapping("/createBasicUser")
	public MSAResponse createBasicUser(@RequestBody UserDTO userDTO) {
		return userService.createBasicUser(userDTO);
	}

	@GetMapping("/userDetails")
	public MSAResponse getAllUserInformations(
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize) {
		return userService.getAllUserInformations(pageNo ,pageSize);
	}

}
