package com.mindzen.planner.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.CompanyUsers;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.entity.UserLicense;
import com.mindzen.planner.repository.CompanyRepository;
import com.mindzen.planner.repository.CompanyUsersRepository;
import com.mindzen.planner.repository.UserLicenseRepository;
import com.mindzen.planner.repository.UserRepository;
import com.mindzen.planner.util.Constants;

public class UserConverter {
	/**
	 * Convert the user entity entity to user dto
	 * @param companyRepository 
	 * @param userRepository 
	 * @param userLicenseRepository 
	 * @param created 
	 * @param companyUsersRepository 
	 */
	public  List<UserDTO> userEntityTOuserDTOConvert(List<User> usersList, CompanyRepository companyRepository,
			UserLicenseRepository userLicenseRepository,UserRepository userRepository, CompanyUsersRepository 
			companyUsersRepository, Long created) {
		Long userId = UserInfoContext.getUserThreadLocal();
		List<UserDTO> userList = new ArrayList<>(); 
		usersList.forEach(user -> {
			UserDTO userDTO = new UserDTO();
			userDTO.setId(user.getId());
			userDTO.setFirstName(user.getFirstName());
			userDTO.setLastName(user.getLastName());
			userDTO.setEmail(user.getEmail().toLowerCase());
			userDTO.setContactNo(user.getContactNo());
			userDTO.setTimeZone(user.getTimeZone());
			List<Long> projectUserIdList = new ArrayList<>();
			List<UserLicense> userLicense  = new ArrayList<>();
			userLicense = userLicenseRepository.findByUser(user);
			projectUserIdList = userRepository.findUserAvailabilityInProjectByUserId(user.getId());
			if(userLicense.isEmpty() && projectUserIdList.isEmpty())
				userDTO.setDelete(true);
			else
				userDTO.setDelete(false);
			if(!user.getId().equals(userId)) {
				CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, userId);
				if(null != companyUsers && null != companyUsers.getCompany()) {
					Company company =  companyUsers.getCompany();
					userDTO.setCompanyId(company.getId());
					userDTO.setCompanyName(company.getName());
					userDTO.setCompanyUserId(companyUsers.getId());
				}
				if(null != companyUsers && (companyUsers.getCreatedBy().equals(1L) || companyUsers.getCreatedBy().equals(userId)))
					userDTO.setEdit(true);
			}
			else {
				Long createdBy = userRepository.findCreatedByByUserId(userId);
				CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user,createdBy);
				if(null != companyUsers && null != companyUsers.getCompany()) {
					Company company =  companyUsers.getCompany();
					userDTO.setCompanyId(company.getId());
					userDTO.setCompanyName(company.getName());
					userDTO.setCompanyUserId(companyUsers.getId());
				}
				if(null != companyUsers && (companyUsers.getCreatedBy().equals(1L) || companyUsers.getCreatedBy().equals(userId)))
					userDTO.setEdit(true);
			}
			userList.add(userDTO);
		});
		return userList;
	}

	/**
	 * Convert the company dto entity to company entity
	 */
	public Company newCompanyCreation(CompanyDTO companyDTO) {
		Company company = new Company();
		company.setName(companyDTO.getName());
		company.setContactNo(companyDTO.getContactNo());
		company.setContactFirstName(companyDTO.getContactFirstName());
		company.setContactLastName(companyDTO.getContactLastName());
		company.setEmail(companyDTO.getEmail().toLowerCase());
		company.setAddress(companyDTO.getAddress());
		company.setIsActive(true);
		return company ;
	}

	/**
	 * Convert the user dto to user entity
	 */
	public User userDTOTOuserEntityConvert(UserDTO userDTO, Company company) {
		User user = new User();
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setContactNo(userDTO.getContactNo());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setActive(true);
		user.setEmailVerified(false);
		user.setAccountExpired(false);
		user.setCredentialsExpired(false);
		user.setRoleName("ADMIN");
		user.setLastPasswordResetDate(new Date());
		user.setTimeZone(Constants.DEFAULT_TIME_ZONE);
		return user;
	}

	public User userUpdateConvert(UserDTO userDTO, Company company, User user) {
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setContactNo(userDTO.getContactNo());
		user.setPassword(userDTO.getPassword());
		user.setActive(true);
		user.setAccountExpired(false);
		user.setCredentialsExpired(false);
		user.setRoleName("ADMIN");
		user.setLastPasswordResetDate(new Date());
		user.setTimeZone(Constants.DEFAULT_TIME_ZONE);
		user.setEmail(userDTO.getEmail());
		return user;
	}

	public UserDTO userTOuserDTOConverter(User user, CompanyRepository companyRepository, 
			UserLicenseRepository userLicenseRepository, UserRepository userRepository, 
			CompanyUsersRepository companyUsersRepository, Long created) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setEmail(user.getEmail().toLowerCase());
		userDTO.setContactNo(user.getContactNo());
		userDTO.setTimeZone(user.getTimeZone());
		List<Long> projectUserIdList = new ArrayList<>();
		List<UserLicense> userLicense  = new ArrayList<>();
		userLicense = userLicenseRepository.findByUser(user);
		projectUserIdList = userRepository.findUserAvailabilityInProjectByUserId(user.getId());
		if(userLicense.isEmpty() && projectUserIdList.isEmpty())
			userDTO.setDelete(true);
		else
			userDTO.setDelete(false);
		Long userId = UserInfoContext.getUserThreadLocal();
		if(!user.getId().equals(userId)) {
			CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user, userId);
			if(null != companyUsers && null != companyUsers.getCompany()) {
				Company company =  companyUsers.getCompany();
				userDTO.setCompanyId(company.getId());
				userDTO.setCompanyName(company.getName());
				userDTO.setCompanyUserId(companyUsers.getId());
			}
			if(null != companyUsers ) {
				if(companyUsers.getCreatedBy().equals(1L) || companyUsers.getCreatedBy().equals(userId))
					userDTO.setEdit(true);
			}
		}
		else {
			Long createdBy = userRepository.findCreatedByByUserId(userId);
			CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user,createdBy);
			if(null != companyUsers && null != companyUsers.getCompany()) {
				Company company =  companyUsers.getCompany();
				userDTO.setCompanyId(company.getId());
				userDTO.setCompanyName(company.getName());
				userDTO.setCompanyUserId(companyUsers.getId());
			}
			if(null != companyUsers && (companyUsers.getCreatedBy().equals(1L) || companyUsers.getCreatedBy().equals(userId)))
				userDTO.setEdit(true);
		}
		userDTO.setActive(user.isActive());
		return userDTO;
	}

	public UserDTO verifyEmail(User user,UserLicenseRepository userLicenseRepository, UserRepository userRepository,
			CompanyUsersRepository companyUsersRepository) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setEmail(user.getEmail().toLowerCase());
		userDTO.setContactNo(user.getContactNo());
		List<Long> projectUserIdList = new ArrayList<>();
		List<UserLicense> userLicense  = new ArrayList<>();
		userLicense = userLicenseRepository.findByUser(user);
		projectUserIdList = userRepository.findUserAvailabilityInProjectByUserId(user.getId());
		if(userLicense.isEmpty() && projectUserIdList.isEmpty())
			userDTO.setDelete(true);
		else
			userDTO.setDelete(false);
		Long createdBy = userRepository.findCreatedByByUserId(user.getId());
		CompanyUsers companyUsers = companyUsersRepository.findCompanyUsersByuserAndCreatedby(user,createdBy);
		if(null != companyUsers && null != companyUsers.getCompany()) {
			Company company =  companyUsers.getCompany();
			userDTO.setCompanyId(company.getId());
			userDTO.setCompanyName(company.getName());
			userDTO.setCompanyUserId(companyUsers.getId());
		}

		userDTO.setActive(user.isActive());
		return userDTO;
	}

}
