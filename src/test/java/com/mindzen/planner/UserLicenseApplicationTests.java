package com.mindzen.planner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mindzen.planner.config.UserInfoContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("junit")
public class UserLicenseApplicationTests {

	@Autowired
	private WebApplicationContext applicationContext;

	public MockMvc mockMvc;

	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
		UserInfoContext.setUserThreadLocal(1L);
	}
	
	@Test
	public void contextLoads() {
	}

}
