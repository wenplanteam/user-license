package com.mindzen.planner.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.User;

public class UserConverterTest {
	
	UserConverter userConverter;
	
	@Before
	public void setUp(){
		this.userConverter = new UserConverter();
	}

	/**
	 * convert the value entity to dto  unit testing
	 */
	@Test
	public void userEntityTOuserDTOConvertUnitTest() {
		List<User> usersList = new ArrayList<>();
		User user = new User();
		user.setContactNo("9487915489");
		user.setEmail("sam@gmail.com");
		user.setPassword("samsam");
		usersList.add(user);
	//	List<UserDTO> returned = userConverter.userEntityTOuserDTOConvert(usersList );
	//	assertEquals(returned.get(0).getContactNo(), user.getContactNo());
	}
	
	/**
	 *  convert the value dto to entity unit testing
	 */
	@Test
	public void userDTOTOuserEntityConvertUnitTest() {
		Company company = new Company();
		company.setName("Demo Pvt Ltd");
		company.setContactNo("948791541");
		UserDTO userDTO =  new UserDTO();
		userDTO.setContactNo("9487915489");
		userDTO.setEmail("demo@gmail.com");
		userDTO.setPassword("samsam");
		User user = userConverter.userDTOTOuserEntityConvert(userDTO, company);
		assertEquals(user.getEmail(), userDTO.getEmail());
	}
	
	/**
	 *  create new company 
	 */
	@Test
	public void newCompanyCreationUnitTest() {
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setName("Demo Pvt Ltd");
		companyDTO.setContactNo("948791541");
		Company company = userConverter.newCompanyCreation(companyDTO);
		assertTrue(company.getName().equals("Demo Pvt Ltd"));
	}
}
