package com.mindzen.planner.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.UserLicenseApplicationTests;
import com.mindzen.planner.dto.CompanyDTO;


/**
 * @author naveenkumar Boopathi
 *
 */
public class CompanyControllerTest extends UserLicenseApplicationTests {

	@Test
	public void loadCompaniesTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/company"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	public void loadCompanyByIdTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/company?id=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	@Test
	public void loadCompanyByCreatedByTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/company?createdBy=true"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	@Test
	public void loadCompanyByPaginationByTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/company?pageNo=1&pageSize=10"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	@Test
	public void createCompanyTest() throws Exception {
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setName("NAVEEN");
		companyDTO.setEmail("naveen@NAVEEN.com");
		companyDTO.setContactNo("9876543210");
		companyDTO.setAddress("COIMBATORE");
		companyDTO.setContactFirstName("NAVEEN");
		companyDTO.setContactLastName("BOOPATHI");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] lookaheadDTOByte = mapper.writeValueAsBytes(companyDTO);
		MvcResult mvcResult =  mockMvc.perform(post("/company")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(lookaheadDTOByte))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MSAResponse msaResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), MSAResponse.class);
		assertEquals("Success",msaResponse.getMessage());
	}
	
	@Test
	public void updateCompanyTest() throws Exception {
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setId(349L);
		companyDTO.setName("NAVEEN");
		companyDTO.setEmail("naveen@NAVEEN.com");
		companyDTO.setContactNo("9076543210");
		companyDTO.setAddress("COIMBATORE");
		companyDTO.setContactFirstName("NAVEEN");
		companyDTO.setContactLastName("BOOPATHI");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] lookaheadDTOByte = mapper.writeValueAsBytes(companyDTO);
		MvcResult mvcResult =  mockMvc.perform(put("/company")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(lookaheadDTOByte))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MSAResponse msaResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), MSAResponse.class);
		assertEquals("Success",msaResponse.getMessage());
	}

	@Test
	public void deleteCompany() throws Exception {
		List<Long> companyIds = new ArrayList<>();
		companyIds.add(349L);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] companyIdsByte = mapper.writeValueAsBytes(companyIds);
		MvcResult mvcResult = mockMvc.perform(delete("/company")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(companyIdsByte))
				.andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus()); 
	}

}
