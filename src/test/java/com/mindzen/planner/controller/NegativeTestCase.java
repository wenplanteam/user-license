package com.mindzen.planner.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.UserLicenseApplicationTests;
import com.mindzen.planner.dto.UserDTO;


/**
 * @author naveenkumar Boopathi
 *
 */
public class NegativeTestCase extends UserLicenseApplicationTests {

	ObjectMapper mapper=new ObjectMapper();
	
	/**
	 * negative test to No record found
	 */
	@Test
	public void loadUserByNegativeIdTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/users?userId=0"))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MSAResponse msaResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), MSAResponse.class);
		assertEquals("Record Not Found",msaResponse.getMessage());
	}
	
	/**
	 * negative test create new user
	 */
	@Test
	public void createUserNegativeTest() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setFirstName("naveen");
		userDTO.setLastName("boopathi");
		userDTO.setCompanyName("mindzen");
		userDTO.setContactNo("9000080000");
		userDTO.setEmail("nkumarb910@gmail.com");
		userDTO.setPassword("12345678");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] userDTOByte = mapper.writeValueAsBytes(userDTO);
		MvcResult mvcResult =  mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(userDTOByte))
				.andReturn();
		MSAResponse msaResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), MSAResponse.class);
		assertEquals("Email Already Exist",msaResponse.getMessage());
	}

}
