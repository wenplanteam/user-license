package com.mindzen.planner.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.UserLicenseApplicationTests;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.UserDTO;


/**
 * @author naveenkumar Boopathi
 *
 */
public class UserControllerTest extends UserLicenseApplicationTests {


	ObjectMapper mapper=new ObjectMapper();

	/**
	 * positive test load all users
	 */
	@Test
	public void loadUsersTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * positive test load particular user
	 */
	@Test
	public void loadUserTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/users?userId=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * positive test create new user
	 */
	@Test
	public void createUserTest() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setFirstName("naveen");
		userDTO.setLastName("boopathi");
		userDTO.setCompanyName("mindzen");
		userDTO.setContactNo("9000080000");
		String email = UUID.randomUUID().toString().replace("-", "");
		userDTO.setEmail(email+"@gmail.com");
		userDTO.setPassword("123");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] userDTOByte = mapper.writeValueAsBytes(userDTO);
		MvcResult mvcResult =  mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(userDTOByte))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * change password service test
	 * @throws Exception
	 */
	@Test
	public void changePasswordTest() throws Exception {
		AuthenticationRequest   authenticationRequest = new AuthenticationRequest();
		authenticationRequest.setNewPassword("Naveen@123");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] authenticationRequestByte = mapper.writeValueAsBytes(authenticationRequest);
		MvcResult mvcResult =  mockMvc.perform(post("/changePassword")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(authenticationRequestByte))
				.andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * delete the user by userId
	 * @throws Exception
	 */
	@Test
	public void deleteUserByIdTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(delete("/user?userId=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * Load user by Email 
	 * @throws Exception
	 */
	@Test
	public void loadUserByEmailTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/findUserByEmail?email=nkumarb910@gmail.com"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * verify email service update 
	 * @throws Exception
	 */
	@Test
	public void verifyEmailTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/verifyEmail?email=nkumarb910@gmail.com&source=newUser"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * create user by using basic information of (email ,lastName, lookahead access)
	 * @throws Exception
	 */
	@Test
	public void createUserByBasicInfoTest() throws Exception {
		List<UserDTO> userDTOs = new ArrayList<>();
		UserDTO userDTO = new UserDTO();
		userDTO.setLastName("Boopathi");
		userDTO.setEmail("nkumarb910@gmail.com");
		String[] array = {"VIEW"};
		userDTO.setLookaheadAccess(array);
		userDTOs.add(userDTO);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] userDTOsByte = mapper.writeValueAsBytes(userDTOs);
		MvcResult mvcResult =  mockMvc.perform(post("/createUserByBasicInfo")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(userDTOsByte))
				.andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}



	/**
	 * @throws Exception
	 */
	@Test
	public void decodeTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/decode?data=bmt1bWFyYjkxMEBnbWFpbC5jb20="))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		MSAResponse msaResponse = mapper.readValue(response.getContentAsString(), MSAResponse.class);
		UserDTO userDTO = mapper.convertValue(msaResponse.getPayload(), UserDTO.class);
		assertEquals("nkumarb910@gmail.com", userDTO.getEmail());
	}
	
	@Test
	public void getProjectUsersTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/projectUsers?projectId=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

}
