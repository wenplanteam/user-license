package com.mindzen.planner.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.UserLicenseApplicationTests;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;
import com.mindzen.planner.entity.UserPlanMaster;

public class LicenseControllerTest extends UserLicenseApplicationTests {

	/*
	 {"amount":8.5,"planDTOs":[{"price":8.5,"noOfLicense":1,"billingCycle":"Monthly","id":2}]} 



	 **/


	@Test
	public void purchaseLicenseTest() throws Exception {
		UserPlanMasterDTO userPlanMasterDTO = new UserPlanMasterDTO();
		userPlanMasterDTO.setAmount(new BigDecimal("8.5"));
		List<PlanDTO> planDTOs = new ArrayList<>();
		PlanDTO planDTO = new PlanDTO();
		planDTO.setPrice(new BigDecimal("8.5"));
		planDTO.setLicensesNos(1L);
		planDTO.setBillingCycle("Monthly");
		planDTO.setId(2L);
		planDTOs.add(planDTO);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] lookaheadDTOByte = mapper.writeValueAsBytes(planDTOs);
		MvcResult buyLicense =  mockMvc.perform(post("/buyLicense")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(lookaheadDTOByte))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse buyLicenseResponse = buyLicense.getResponse();
		MSAResponse msaResponse = mapper.readValue(buyLicense.getResponse().getContentAsString(), MSAResponse.class);
		UserPlanMaster userPlanMaster = mapper.convertValue(msaResponse.getPayload(), UserPlanMaster.class);
		MvcResult licensepayResponse = mockMvc.perform(get("/licenseResponse?transactionId"+userPlanMaster.getTransactionId()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse licenseResponse = licensepayResponse.getResponse();
		assertSame(buyLicenseResponse.getStatus(),licenseResponse.getStatus());
	}
}
