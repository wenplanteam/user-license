package com.mindzen.planner.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.springframework.test.web.servlet.MockMvc;

import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.converter.UserConverter;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.Company;
import com.mindzen.planner.entity.User;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.CompanyRepository;
import com.mindzen.planner.repository.LicenceDistributionRepositpry;
import com.mindzen.planner.repository.PlanRepository;
import com.mindzen.planner.repository.UserPlanDetailRepository;
import com.mindzen.planner.repository.UserRepository;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
/**
 *  @author Naveenkumar Boopathi
 *
 */
public class UserServiceImplTest {

	//@Resource
	//private WebApplicationContext applicationContext;

	public MockMvc mockMvc;
	UserRepository userRepository;
	CompanyRepository companyRepository;
	LicenceDistributionRepositpry licenceDistributionRepositpry;
	PlanRepository planRepository;
	UserPlanDetailRepository userPlanDetailRepository;
	UserServiceImpl userServiceImpl;
	ModelMapper modelMapper;
	UserConverter userConverter;
	MessageQueueService messageQueueService;
	AppConfig appConfig;

	/**
	 *  mock initialization
	 */
	@Before
	public void setUp(){
		//mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
		userRepository = mock(UserRepository.class);
		companyRepository = mock(CompanyRepository.class);
		modelMapper = mock(ModelMapper.class);
		licenceDistributionRepositpry = mock(LicenceDistributionRepositpry.class);
		userPlanDetailRepository = mock(UserPlanDetailRepository.class);
		planRepository = mock(PlanRepository.class);
		messageQueueService = mock(MessageQueueService.class);
		appConfig = mock(AppConfig.class);
		userServiceImpl = new UserServiceImpl(userRepository, companyRepository,  modelMapper,planRepository,licenceDistributionRepositpry,userPlanDetailRepository, messageQueueService, appConfig, null, null, null, null, null, null, null, null, null);
		this.userConverter = new UserConverter();
		
	}

	/**
	 * create new user unit testing
	 */
/*	@Test
	public void newUserCreationUnitTest() throws Exception {

		Company persisted = new Company();
		persisted.setName("Demo Pvt Ltd");
		persisted.setContactNo("948791541");

		when((appConfig.getNewUserType())).thenReturn("newUser");
		when((appConfig.getUserVerifyPath())).thenReturn("/api/emzee/auth/verifyEmail?source=$source$&email=$email$");

		when(companyRepository.save(any(Company.class))).thenReturn(persisted);

		User user = new User();
		user.setEmail("mindz@gmail.com");
		user.setFirstName("sample");
		user.setLastName("samples");
		user.setPassword("boopathi");
		user.setContactNo("9487915489");

		when(userRepository.save(any(User.class))).thenReturn(user);

		UserDTO userDTO = new UserDTO();
		userDTO.setCompanyName("Demo Pvt Ltd");
		userDTO.setEmail("mindz@gmail.com");
		userDTO.setFirstName("sample");
		userDTO.setLastName("samples");
		userDTO.setPassword("boopathi");
		userDTO.setContactNo("9487915489");
		MSAResponse status = userServiceImpl.createUser(userDTO);

		ArgumentCaptor<User> userArgument = ArgumentCaptor.forClass(User.class);
		verify(userRepository, times(1)).save(userArgument.capture());
		verifyNoMoreInteractions(userRepository);
		assertEquals(userDTO.getLastName(), userArgument.getValue().getLastName());
		assertEquals(true, status);
	}*/

	/**
	 * new company creation api +unit testing
	 */
	@Test
	public void newCompanyCreationUnitTest() throws Exception {

		Company persisted = new Company();
		persisted.setName("Demo Pvt Ltd");
		persisted.setContactNo("948791541");

		when(companyRepository.save(any(Company.class))).thenReturn(persisted);

		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setName("Demo Pvt Ltd");
		companyDTO.setContactNo("948791541");

		Company returned = userConverter.newCompanyCreation(companyDTO);
		returned = companyRepository.save(returned);

		ArgumentCaptor<Company> companyArgument = ArgumentCaptor.forClass(Company.class);
		verify(companyRepository, times(1)).save(companyArgument.capture());
		verifyNoMoreInteractions(companyRepository);
		assertEquals(companyDTO.getName(), companyArgument.getValue().getName());
		assertEquals(persisted, returned);
	}

	/**
	 * load the data when Load user GET method called
	 */
	@Test
	public void usersUnitTest() throws Exception {
		User user = new User();
		user.setEmail("mindz@gmail.com");
		user.setFirstName("sample");
		user.setLastName("samples");
		user.setPassword("boopathi");
		user.setContactNo("9487915489");
		List<User> usersLists = new ArrayList<>();
		usersLists.add(user);
        when(userRepository.findAll()).thenReturn(usersLists);
        List<User> users = new ArrayList<>();
        users = userRepository.findAll();
        assertEquals(user.getContactNo(), users.get(0).getContactNo());
	}
	
	/**
	 * load the data when update user method called
	 */
	@Test
	public void updateUserUnitTest() throws Exception {
		User user = new User();
		user.setEmail("mindz@gmail.com");
		user.setFirstName("sample");
		user.setLastName("samples");
		user.setPassword("boopathi");
		user.setContactNo("9487915489");
		UserDTO userDTO = new UserDTO();
		userDTO.setId(1L);
        when(userRepository.getOne(userDTO.getId())).thenReturn(user);
 //       MSAResponse result = userServiceImpl.updateUser(userDTO);
//        assertEquals(result.getMessage(),ApiResponseConstants.SUCS);
	}
	
	/**
	 *  load the data when delete user method called
	 */
	@Test
	public void deleteUserUnitTest() throws Exception {
		User user = new User();
		user.setEmail("mindz@gmail.com");
		user.setFirstName("sample");
		user.setLastName("samples");
		user.setPassword("boopathi");
		user.setContactNo("9487915489");
		UserDTO userDTO = new UserDTO();
		userDTO.setId(1L);
        when(userRepository.getOne(userDTO.getId())).thenReturn(user);
    //    MSAResponse result = userServiceImpl.updateUser(userDTO);
 //       assertEquals(result.getMessage(),ApiResponseConstants.SUCS);
	}

}
