
    create table company (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        address varchar(255),
        contact_first_name varchar(255),
        contact_last_name varchar(255),
        contact_no varchar(255) not null,
        email varchar(255),
        is_active boolean not null,
        name varchar(255) not null,
        primary key (id)
    );
    create table license_distribution (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        is_active boolean not null,
        license_to_user_id int8,
        remaining_projects_count int8 not null,
        total_projects_count int8 not null,
        user_plan_id int8,
        primary key (id)
    );
    create table master (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        description varchar(255) not null,
        primary key (id)
    );
    create table master_detail (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        master_id int8 not null,
        value varchar(255) not null,
        primary key (id)
    );
    create table payment (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        amount varchar(255),
        currency varchar(255),
        new_plan_id int8,
        old_plan_id int8,
        status varchar(255),
        transaction_date timestamp not null,
        transaction_id int8,
        primary key (id)
    );
    create table plan (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        description varchar(255),
        is_active boolean not null,
        name varchar(255) not null,
        price varchar(255) not null,
        projects_count int8 not null,
        primary key (id)
    );
    create table user_plan (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        active_from timestamp,
        active_to timestamp,
        is_active boolean not null,
        plan_id int8,
        total_purchased_licenses int8,
        total_used_licenses int8,
        user_id int8,
        primary key (id)
    );
    create table users (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        account_expired boolean,
        company_id int8,
        contact_no varchar(255),
        credentials_expired boolean,
        email varchar(255) not null,
        email_verified boolean,
        first_name varchar(255),
        is_active boolean not null,
        last_name varchar(255) not null,
        last_password_reset_date timestamp,
        password varchar(255),
        role_name varchar(255),
        primary key (id)
    );

    CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
  
    CREATE SEQUENCE public.users_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

    CREATE SEQUENCE public.USER_PLAN_ID_SEQ
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
  
    CREATE SEQUENCE public.license_distribution_id_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

    create table project_users (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        access varchar(255),
        is_active boolean not null,
        is_deleted boolean,
        project_role varchar(255),
        user_id int8 not null,
        project_id int8 not null,
        project_company_id int8,
        primary key (id)
    );
	
    CREATE SEQUENCE public.project_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

    create table projects (
        id  bigserial not null,
        created_by int8,
        created_on timestamp,
        modified_by int8,
        modified_on timestamp,
        anchor_date timestamp,
        anchor_week varchar(255),
        city varchar(255),
        country varchar(255),
        county varchar(255),
        description varchar(255),
        estimated_end_date timestamp,
        estimated_start_date timestamp,
        is_active boolean not null,
        is_deleted boolean,
        job_phone varchar(255),
        latitude int8,
        leave_days varchar(255),
        license_distribution_id int8 not null,
        longitude int8,
        lookahead_notification boolean,
        name varchar(255),
        owner_company varchar(255),
        project_manager varchar(255) not null,
        project_no varchar(255) not null,
        project_stage varchar(255),
        show_project_address boolean,
        state varchar(255),
        timezone varchar(255),
        week_start_day varchar(255),
        zip_code varchar(255),
        primary key (id)
    );

    CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


